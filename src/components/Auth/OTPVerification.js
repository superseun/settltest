import React, { Component } from "react";
import { Modal, ModalBody } from "reactstrap";
import "./ForgetPasword.css";
import logo from "../../assets/img/icons/close.svg";
import OtpInput from 'react-otp-input';

class OTPVerification extends Component {
  state = {
    modal: false,
    otp: '',
  };
  state = {
    options: [
      { name: "Admin", id: 1 },
      { name: "Super Admin", id: 2 },
      { name: "Customer Sucess", id: 3 },
    ],
  };

  handleChange = otp => this.setState({ otp });

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };

  render() {
    return (
      <div>
        <button className="text-center btn btn_continue" onClick={this.toggle}>Continue</button>
        <Modal
          id="modal_section"
          isOpen={this.state.modal}
          toggle={this.toggle}
          size="lg"
          centered
        >
          <div data-test="modal-header" id="close" className="modal-header">
            <h4 className="modal-title">
              <div className="text-center forget_pass_title">
                <h1>OTP Verification</h1>
              </div>
            </h4>
            <button
              onClick={this.toggle}
              type="button"
              className="close"
              aria-label="Close"
            >
              <img src={logo} alt="close" />
            </button>
          </div>

          <ModalBody>
            <div className="">
              <div className="code_otp">
              <span>Enter the otp code sent to</span>
              <br/>
              <span><b>Assuranceuwangue@gmail.com</b></span>
              </div>
                
                <div className="otp_con_inp">
                <OtpInput className="otp_sty"

                    onChange={this.handleChange}
                    numInputs={6}
                    value={this.state.otp}
                    otpType="password"
                    disabled={false}
                    secure={true}
                    separator={<span></span>}
                />
                </div>

              <div>
       
        </div>

              <button className="text-center btn btn_continue">Continue</button>

        
            </div>
          </ModalBody>
          {/* <div className="modal-footer forget_pass_modal_footer"></div>
          <div className="forget_pass_footer_section">
            <button
              type="button"
              className="btn forget_pass_cancel"
              onClick={this.toggle}
            >
              Cancel
            </button>
            <button type="button" className="btn forget_pass_submit">
              Submit Details
            </button>
          </div> */}
        </Modal>
      </div>
    );
  }
}

export default OTPVerification;
