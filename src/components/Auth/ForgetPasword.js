import React, { Component } from "react";
import { Modal, ModalBody } from "reactstrap";
import "./ForgetPasword.css";
import logo from "../../assets/img/icons/close.svg";
import { Multiselect } from "multiselect-react-dropdown";

class ForgetPassword extends Component {
  state = {
    modal: false,
  };
  state = {
    options: [
      { name: "Admin", id: 1 },
      { name: "Super Admin", id: 2 },
      { name: "Customer Sucess", id: 3 },
    ],
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };

  render() {
    return (
      <div>
        <span className="forget_login" onClick={this.toggle}>
          Forget pasword?
        </span>
        <Modal
          id="modal_section"
          isOpen={this.state.modal}
          toggle={this.toggle}
          size="lg"
          centered
        >
          <div data-test="modal-header" id="close" className="modal-header">
            <h4 className="modal-title">
              <div className="text-center forget_pass_title">
                <h1>Forgot Password</h1>
              </div>
            </h4>
            <button
              onClick={this.toggle}
              type="button"
              className="close"
              aria-label="Close"
            >
              <img src={logo} alt="close" />
            </button>
          </div>

          <ModalBody>
            <div className="well">
              <p>
                Provide your admin details which will be sent to a supper admin
                for your password update
              </p>
              <fieldset className="float-label">
                <input
                  name="Full Name"
                  autoComplete="off"
                  type="text"
                  className="form-control shadow-none"
                  required
                />
                <label htmlFor="Full_Name">Full Name</label>
              </fieldset>
              <fieldset className="float-label">
                <input
                  name="Email address"
                  autoComplete="off"
                  type="text"
                  className="form-control shadow-none"
                  required
                />
                <label htmlFor="Email_address">Email address</label>
              </fieldset>

              <Multiselect
                className="forget_pass_select"
                options={this.state.options} // Options to display in the dropdown
                showCheckbox={true}
                showArrow={true}
                style={{
                  border: "3px solid red",
                  "border-bottom": "1px solid blue",
                  "border-radius": "0px",
                }}
                selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                onSelect={this.onSelect} // Function will trigger on select event
                onRemove={this.onRemove} // Function will trigger on remove event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Admin Role"
                id="css_custom"
              />
            </div>
          </ModalBody>
          <div className="modal-footer forget_pass_modal_footer"></div>
          <div className="forget_pass_footer_section">
            <button
              type="button"
              className="btn forget_pass_cancel"
              onClick={this.toggle}
            >
              Cancel
            </button>
            <button type="button" className="btn forget_pass_submit">
              Submit Details
            </button>
          </div>
        </Modal>
      </div>
    );
  }
}

export default ForgetPassword;
