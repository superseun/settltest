import React from "react";

import {
  Col,
  Row,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Calendar } from "react-feather";
const Header = ({ logo, name, calendar }) => (
  <Row className="mb-2 mb-xl-4">
    <Col xs="auto" className="d-none d-sm-block">
      <h4 className="greeting">
        <img src={logo} className="" alt="Settl Logo" />
        <span className="pl-2">{name}</span>
      </h4>
    </Col>

    {calendar && (
      <Col xs="auto" className="ml-auto text-right mt-n1 pr-3">
        <UncontrolledDropdown className="d-inline filter-dropdown">
          <DropdownToggle
            caret
            color="light"
            className="shadow-sm"
            style={{
              backgroundColor: "white",
            }}
          >
            <Calendar className="feather align-middle mt-n1" /> Last 30 days
          </DropdownToggle>
          <DropdownMenu
            right
            style={{
              top: "25px",
            }}
          >
            <DropdownItem>Today</DropdownItem>
            <DropdownItem>Last 7 Days</DropdownItem>
            <DropdownItem>Last 90 Days</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Customize</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </Col>
    )}
  </Row>
);
export default Header;
