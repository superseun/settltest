import React, { useState } from "react";
import { Badge, Button } from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import Loader from "./../../components/Loader";
import MessageTableHeader from "./MessageTableHeader";
import CreateNewMessage from "./CreateNewMessage";
import { Plus } from "react-feather";

const tableData = [
    {
        id: 1,
        date: "10-01-2020",
        messageType: "Email",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Sent"
    },
    {
        id: 2,
        date: "10-01-2020",
        messageType: "Email",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Sent"
    },
    {
        id: 3,
        date: "10-01-2020",
        messageType: "Email",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Draft"
    },
    {
        id: 4,
        date: "10-01-2020",
        messageType: "Email",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Draft"
    },
    {
        id: 5,
        date: "10-01-2020",
        messageType: "Email",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Sent"
    },
    {
        id: 6,
        date: "10-01-2020",
        messageType: "SMS",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Sent"
    },
    {
        id: 7,
        date: "10-01-2020",
        messageType: "Email",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Failed"
    },
    {
        id: 8,
        date: "10-01-2020",
        messageType: "SMS",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Sent"
    },
    {
        id: 9,
        date: "10-01-2020",
        messageType: "Email",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Sent"
    },
    {
        id: 10,
        date: "10-01-2020",
        messageType: "Email",
        sender: "Carolyn Harvey",
        title: "Settl Campaign",
        message: "Lorem ipsum dolor sit amet... ",
        status: "Sent"
    }
]

const tableColumns = [
    {
      dataField: "date",
      text: "Date"
    },
    {
        dataField: "messageType",
        text: "Type"
    },
    {
        dataField: "sender",
        text: "Sender"
    },
    {
        dataField: "title",
        text: "Title"
    },
    {
        dataField: "message",
        text: "Message",
    },
    {
      dataField: "status",
      text: "Status",
      headerClasses: "text-center",
      formatter: (cell, row, rowIndex) => {
        if (cell.toLowerCase() === "sent") {
            return <Badge color="secondary" className="badge-active">{cell}</Badge>
        }
        if (cell.toLowerCase() === "draft") {
            return <Badge color="secondary" className="badge-pending">{cell}</Badge>
        }
        if (cell.toLowerCase() === "failed") {
            return <Badge color="secondary" className="badge-unresolved">{cell}</Badge>
        }
      }
    }
];

export default function MessageTable() {
    const [showNewMessage, setShowNewMessage] = useState(false);
    const [isfiltered, setIsFiltered] = useState(false);
    const [data, setData] = useState(tableData);

    const Filter = (data) => {
        setData("");
        const check = data.status.length > 0
    
        setTimeout(() => {
          const filtered = tableData.filter((product) => {
            if (check) {
              setIsFiltered(true);
              return (
                data.status.includes(product.status.toLowerCase())
              );
            } else {
              setIsFiltered(false);
              return product;
            }
          });
          setData(filtered);
        }, 500);
    };

    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total">
            Showing { from } to { to } of { size } Results
        </span>
    );

    const rowEvents = {
        onClick: (e, row, rowIndex) => {
            console.log(rowIndex);
        }
    }

    return(
        <>
            {!showNewMessage ? (
                <div className="admin-body">
                    <div className="all-admins" style={{ position: "relative" }}>
                        <h6>All Messages</h6>
                        <div className="text-right message-right">
                            <Button 
                                color="primary" 
                                className="btn-add"
                                onClick={() => setShowNewMessage(true)}
                            >
                                <Plus /> New Message
                            </Button>
                        </div>
                        <div className="admin-table message-table mt-4">
                            <ToolkitProvider
                                keyField="id"
                                data={data}
                                columns={tableColumns}
                            >
                                {
                                    props => (
                                    <div>
                                        <MessageTableHeader
                                            filter={Filter}
                                            filtered={isfiltered}
                                            dataLength={data.length}
                                        />
                                        {!data ? (
                                            <Loader />
                                        ) :(
                                            <BootstrapTable
                                                bootstrap4
                                                bordered={false}
                                                rowEvents={rowEvents}
                                                wrapperClasses="table-responsive"
                                                pagination={paginationFactory({
                                                    sizePerPage: 9,
                                                    hideSizePerPage: true,
                                                    showTotal: true,
                                                    paginationTotalRenderer: customTotal,
                                                })}
                                                { ...props.baseProps }
                                            />
                                        )}
                                    </div>)
                                }
                            </ToolkitProvider>
                        </div>
                    </div>
                </div>
            ) : (
                <CreateNewMessage setShowNewMessage={setShowNewMessage} />  
            )}
        </>
    );
}
