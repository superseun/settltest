import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Card, Form, FormGroup, Label, Input } from "reactstrap";
import { Multiselect } from "multiselect-react-dropdown";
import InputBox from "./../../components/InputBox";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import SuccessDialog from "./SuccessDialog";
import FailureDialog from "./FailureDialog";
import pdf from "../../assets/img/icons/pdf.svg";

const messageType = [
    { name: "Email" },
    { name: "SMS" }
] 

const recipient = [
    { name: "All Customers" },
    { name: "All Agents" }, 
    { name: "Churn Customers" },
    { name: "Customized Users" }
]

export default function CreateNewMessage({setShowNewMessage}) {
    const [title, setTitle] = useState("Settl Easter Campaign");

    const [showSuccess, setShowSuccess] = useState(false);
    const [showFailure, setShowFailure] = useState(false);
    const [selectedValues, setSelectedValues] = useState("");
    const [customerTemplate, setCustomerTemplate] = useState("");

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);


    return(
        <>
            <Link 
                to="/messages"
                className="back"
                onClick={() => setShowNewMessage((prev) => !prev)}
            >
                &lt; Back to All Messages
            </Link>
            <div className="w-50 mx-auto">
                <Card className="p-4" style={{border: "1px solid #ECF3FC"}}>
                    <h6 className="text-center">New Message</h6>
                    <Form className="message-form mt-4">
                        <FormGroup>
                            <Multiselect
                                className="forget_pass_select"
                                options={messageType}
                                showCheckbox={false}
                                showArrow={true}
                                displayValue="name"
                                placeholder="Message Type"
                                singleSelect={true}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Multiselect
                                className="forget_pass_select"
                                options={recipient}
                                showCheckbox={false}
                                showArrow={true}
                                displayValue="name"
                                placeholder={selectedValues === "" ? "Send to" : selectedValues}
                                singleSelect={true}
                                onSelect={(e) => setSelectedValues(e[0].name)}
                            />
                        </FormGroup>
                        {(customerTemplate === "" && selectedValues === "Customized Users") && (
                            <FormGroup className="upload">
                                <div>
                                    <Label htmlFor="customerTemplate">Customer</Label>
                                    <Input 
                                        type="file" 
                                        id="customerTemplate" 
                                        accept=".png, .jpg, .pdf, .csv" 
                                        value={customerTemplate}
                                        onChange={(e) => setCustomerTemplate(e.target.value)}
                                    />
                                </div>
                                <span className="d-block pt-2">
                                    Max file size should be 20MB. 
                                    <a href="#" download>Download this template format</a>
                                </span>
                            </FormGroup>
                        )}
                        {customerTemplate !== "" && (
                            <FormGroup className="upload">
                                <div>
                                    <Label htmlFor="customerTemplate">Customer</Label>
                                </div>
                                <div className="document-wrapper align-items-center d-flex">
                                    <div className="mr-3">
                                        <img src={pdf} alt="pdf icon" />
                                    </div>
                                    <div>
                                        <p className="mb-0">
                                            {customerTemplate.replace(/^C:\\fakepath\\/, "")}
                                        </p>
                                    </div>
                                </div>
                            </FormGroup>
                        )}
                        <FormGroup className="pb-3 relative">
                            <InputBox
                                type="text"
                                name="title"
                                id="title"
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                            />
                            <Label for="title" className="label-title">Title</Label>
                        </FormGroup>
                        <FormGroup>
                            <Editor
                                toolbarClassName="toolbarClassName"
                                wrapperClassName="wrapperClassName"
                                editorClassName="editorClassName"
                                toolbarClassName="toolbar-class"
                                toolbar={{
                                    options: ['inline', 'link', 'textAlign', 'list', 'image'],
                                    inline: { 
                                        inDropdown: false,
                                        options: ['bold', 'italic']
                                    },
                                    textAlign: { inDropdown: false },
                                    link: { inDropdown: false }
                                }}
                            />
                            <div className="mt-1 text-right">
                                <p style={{opacity: "0.7"}}>Max 366 Char</p>
                            </div>
                        </FormGroup>
                        <Button 
                            color="primary" 
                            className="btn-add py-2 w-100"
                            onClick={() => setShowFailure(true)}
                        >
                            Send Message
                        </Button>
                    </Form>
                </Card>
            </div>

            <SuccessDialog 
                isOpen={showSuccess}
                toggle={() => setShowSuccess(!showSuccess)}
            />

            <FailureDialog 
                isOpen={showFailure}
                toggle={() => setShowFailure(!showFailure)}
            />
        </>
    );
}