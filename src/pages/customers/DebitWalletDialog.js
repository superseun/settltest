import React, { useState } from "react";
import { 
    Button,
    Modal, 
    ModalBody,
    ModalHeader,
    Form,
    FormGroup,
    Label 
} from "reactstrap";
import InputBox from "../../components/InputBox";

export default function DebitWalletDialog(props) {
    const [transactionRef, setTransactionRef] = useState("Transaction Refrenence");
    const [amount, setAmount] = useState("₦70,000.00");
    const [reason, setReason] = useState("");
    
    return(
        <>
            <Modal isOpen={props.isOpen} toggle={props.toggle} className="admin-modal freeze-modal">
                <ModalHeader>Debit Customer Wallet </ModalHeader>
                <ModalBody>
                    <div>
                        <div className="text-center">
                            <p>You are about to fund a customer wallet</p>
                        </div>
                        <Form>
                            <FormGroup>
                                <InputBox
                                    type="text"
                                    name="transactionRef" 
                                    id="transactionRef"
                                    value={transactionRef}
                                    onChange={(e) => setTransactionRef(e.target.value)}
                                />
                                <Label for="transactionRef">Transaction Reference</Label>
                            </FormGroup>
                            <FormGroup>
                                <InputBox
                                    type="text"
                                    name="amount"
                                    id="amount"
                                    value={amount}
                                    onChange={(e) => setAmount(e.target.value)}
                                />
                                <Label for="amount">Amount</Label>
                            </FormGroup>
                            <FormGroup className="pb-3">
                                <InputBox
                                    type="textarea"
                                    name="reason"
                                    id="reason"
                                    value={reason}
                                    rows="4"
                                    onChange={(e) => setReason(e.target.value)}
                                />
                                <Label for="reason">Reason why this is been initiated...</Label>
                            </FormGroup>
                        </Form>
                        <hr/>
                        <div className="d-flex justify-content-between">
                            <Button color="info" className="btn-cancel" onClick={props.toggle}>Cancel</Button>
                            <Button color="primary" className="btn-add">Debit Wallet</Button>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}
