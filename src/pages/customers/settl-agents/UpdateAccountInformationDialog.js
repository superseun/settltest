import React, { useState } from "react";
import { ReactComponent as Close } from "../../../BgImages/close.svg";
import { 
    Button,
    Modal, 
    ModalBody,
    ModalHeader,
    Form,
    FormGroup,
    Label
} from "reactstrap";
import InputBox from "../../../components/InputBox";

export default function UpdateAccountInformationDialog(props) {
    const [firstName, setFirstName] = useState("Assurance");
    const [lastName, setLastName] = useState("Uwangue");
    const [emailAddress, setEmailAddress] = useState("Assurance.uwangue@thebulb.africa");
    const [phoneNumber, setPhoneNumber] = useState("09029244116");

    return(
        <>
            <Modal isOpen={props.isOpen} toggle={props.toggle} className="admin-modal suspended-modal">
                <ModalHeader>
                    Update Account Information
                    <Close
                        className="close-modal"
                        onClick={props.toggle}
                    />
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="firstName" 
                                id="firstName"
                                value={firstName}
                                onChange={(e) => setFirstName(e.target.value)}
                            />
                            <Label for="firstName">First Name</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="lastName" 
                                id="lastName"
                                value={lastName}
                                onChange={(e) => setLastName(e.target.value)}
                            />
                            <Label for="lastName">First Name</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="emailAddress" 
                                id="emailAddress"
                                value={emailAddress}
                                onChange={(e) => setEmailAddress(e.target.value)}
                            />
                            <Label for="emailAddress">First Name</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="phoneNumber" 
                                id="phoneNumber"
                                value={phoneNumber}
                                onChange={(e) => setPhoneNumber(e.target.value)}
                            />
                            <Label for="adminFullName">First Name</Label>
                        </FormGroup>
                    </Form>
                    <br/><br/><br/><br/>
                    <hr/>
                    <div className="d-flex justify-content-between">
                        <Button color="info" className="btn-cancel" onClick={props.toggle}>Cancel</Button>
                        <Button color="primary" className="btn-add">Update</Button>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}
