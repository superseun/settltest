import React, { useState } from "react";
import { 
    Button,
    Modal, 
    ModalBody,
    ModalHeader,
    Form,
    FormGroup,
    Label 
} from "reactstrap";
import InputBox from "../../../components/InputBox";
import securityIcon from "./../../../assets/img/icons/security-warning.svg";

export default function BlacklistDeviceDialog(props) {
    const [adminFullName, setAdminFullName] = useState("Assurance uwangue");
    const [reasonForBlacklist, setReasonForBlacklist] = useState("Fraud suspected");
    
    return(
        <>
            <Modal isOpen={props.isOpen} toggle={props.toggle} className="admin-modal freeze-modal">
                <ModalHeader>Blacklist Device</ModalHeader>
                <ModalBody>
                    <div>
                        <div className="text-center">
                            <img src={securityIcon} className="mb-3" alt="security warning icon" />
                            <p>Are you sure you want to blacklist the agent device connected to this account?</p>
                        </div>
                        <Form>
                            <FormGroup>
                                <InputBox
                                    type="text"
                                    name="adminFullName" 
                                    id="adminFullName"
                                    value={adminFullName}
                                    onChange={(e) => setAdminFullName(e.target.value)}
                                />
                                <Label for="adminFullName">Admin Full Name</Label>
                            </FormGroup>
                            <FormGroup className="pb-3">
                                <InputBox
                                    type="textarea"
                                    name="reasonForBlacklist"
                                    id="reasonForBlacklist"
                                    value={reasonForBlacklist}
                                    rows="4"
                                    onChange={(e) => setReasonForBlacklist(e.target.value)}
                                />
                            </FormGroup>
                        </Form>
                        <hr/>
                        <div className="d-flex justify-content-between">
                            <Button color="info" className="btn-cancel" onClick={props.toggle}>Cancel</Button>
                            <Button color="danger" className="btn-delete" onClick={props.onClick}>Yes, Blacklist</Button>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}
