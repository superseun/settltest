import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { 
    Badge,
    Button,
    Card,
    Row, Col, 
    Nav, NavItem, 
    NavLink,
    TabContent,
    TabPane
} from "reactstrap";
import classnames from 'classnames';
import user from "../../../assets/img/icons/user.svg";
import account from "../../../assets/img/icons/account.svg";
import transaction from "../../../assets/img/icons/transaction.svg";
import wallet from "../../../assets/img/icons/wallets.svg";
import referrals from "../../../assets/img/icons/referrals.svg";
import issue from "../../../assets/img/icons/issue-log.svg";
import frozen from "../../../assets/img/icons/frozen.svg";
import MoreDropDown from "./MoreDropDown";
import UpdateDropDown from "./UpdateDropDown";
import ResetButton from "./ResetButton";
import ReferralTable from "./ReferralTable";
import TransactionHistoryTable from "./TransactionHistoryTable";
import CustomerWalletTab from "./CustomerWalletTab";
import ResetPinDialog from "../ResetPinDialog";
import TransactionHistoryDetails from "./TransactionHistoryDetails";
import ReasonForSuspendedAccountDialog from "../ReasonForSuspendedAccountDialog";
import IssueLogTable from "../IssueLogTable";

const accountInfo = [
    {
        id: "1",
        title: <div className="d-flex">
            <p className="mb-0 mr-2">User ID</p>
            <Badge className="badge-referred">Referred</Badge>
        </div>,
        value: "12564"
    },
    {
        id: "2",
        title: "First Name",
        value: "Assurance"
    },
    {
        id: "3",
        title: "Last Name",
        value: "Uwangue"
    },
    {
        id: "4",
        title: "Gender",
        value: "Male"
    },
    {
        id: "5",
        title: "Phone Number",
        value: "+234 7062 390 301"
    },
    {
        id: "6",
        title: "BVN",
        value: "2203564890"
    },
    {
        id: "7",
        title: "Date Created",
        value: "10-03-2021"
    },
    {
        id: "8",
        title: "Number of referral",
        value: "4"
    },
]

const nextOfKinInfo = [
    {
        id: "1",
        title: "Full Name",
        value: "Evidence Uwangue"
    },
    {
        id: "2",
        title: "Email Address",
        value: "Uwangue"
    },
    {
        id: "3",
        title: "Phone Number",
        value: "08052498701"
    },
    {
        id: "4",
        title: "House Address",
        value: "5 Gongola Street, Garki 1, Abuja"
    }
]

export default function CustomerDetails({setShowDetails}) {
    const [activeTab, setActiveTab] = useState('1');
    const [resetCustomerPin, setResetCustomerPin] = useState(false);
    const [suspendedAccount, setSuspendedAccount] = useState(false);
    const [freeze, setFreeze] = useState(false);
    const [freezeModal, setFreezeModal] = useState(false);
    const [successModal, setSuccessModal] = useState(false);
    const [showTransactionDetails, setShowTransactionDetails] = useState(false);

    const toggleFreezeModal = () => setFreezeModal(!freezeModal);
    const toggleSuccessModal = () => setSuccessModal(!successModal);
    const toggleResetCustomerPinModal = () => setResetCustomerPin(!resetCustomerPin);
    const toggleSuspendedAccountModal = () => setSuspendedAccount(!suspendedAccount);

    const freezeAccount = () => {
        setFreeze(true);
        setFreezeModal(false);
        setSuccessModal(true);
    }

    const toggle = tab => {
        if(activeTab !== tab) setActiveTab(tab);
    }

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    
    return(
        <>
            {!showTransactionDetails ? (
                <>
                    <Link 
                        to="/customers/settl-customers"
                        className="back"
                        onClick={() => setShowDetails((prev) => !prev)}
                    >
                        Back to All Settl Customers
                    </Link>
                    
                    <div className="customer-card text-center">
                        <Row>
                            <Col md={4} lg={4}>
                                { freeze ? (
                                    <div className="alert-wrapper">
                                        <div className="d-flex">
                                            <div className="mr-3">
                                                <img src={frozen} alt="an icon" />
                                            </div>
                                            <div className="mt-1">
                                                <h6>Account Frozen</h6>
                                                <p>
                                                    The reason why this account was suspended stays here... 
                                                    <Button color="link" onClick={() => setSuspendedAccount(true)}>Learn more</Button>
                                                </p>
                                            </div>
                                        </div>
                                    </div>) : ""
                                }
                                <Card className="p-3">
                                    <div className="customer-card-header">
                                        <div className="text-right">
                                            <MoreDropDown
                                                isOpen={freezeModal} 
                                                toggle={toggleFreezeModal}
                                                onClick={freezeAccount}
                                                isOpenSuccess={successModal}
                                                toggleSuccess={toggleSuccessModal}
                                                freeze={freeze}
                                            />
                                        </div>
                                        <img src={user} alt="profile"/>
                                        <h6 className="mt-3">Customer Name</h6>
                                        <div className="badge badge-level">KYC Level 1</div>
                                        <span className="d-block mt-3">Available Balance</span>
                                        <p>N1000,000.00</p>
                                    </div>
                                    <Nav tabs>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: activeTab === '1' })}
                                                onClick={() => { toggle('1'); }}
                                            >
                                                <div className="icon-wrapper">
                                                    <img src={account} alt="an icon" />
                                                </div>
                                                Account Information
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: activeTab === '2' })}
                                                onClick={() => { toggle('2'); }}
                                            >
                                                <div className="icon-wrapper">
                                                    <img src={transaction} alt="an icon" />
                                                </div>
                                                Transaction History
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: activeTab === '3' })}
                                                onClick={() => { toggle('3'); }}
                                            >
                                                <div className="icon-wrapper">
                                                    <img src={wallet} alt="an icon" />
                                                </div>
                                                Customer Wallet
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: activeTab === '4' })}
                                                onClick={() => { toggle('4'); }}
                                            >
                                                <div className="icon-wrapper">
                                                    <img src={referrals} alt="an icon" />
                                                </div>
                                                Referrals
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: activeTab === '5' })}
                                                onClick={() => { toggle('5'); }}
                                            >
                                                <div className="icon-wrapper">
                                                    <img src={issue} alt="an icon" />
                                                </div>
                                                Issue Log
                                            </NavLink>
                                        </NavItem>
                                    </Nav>
                                </Card>
                            </Col>
                            <Col md={8} lg={8}>
                                <Card style={{background: "transparent", boxShadow: "none"}}>
                                    <TabContent activeTab={activeTab}>
                                        <TabPane tabId="1" style={{background: "#fff", boxShadow: "0 0 0.875rem 0 rgb(53 64 82 / 5%"}}>
                                            <Row>
                                                <Col sm="12">
                                                    <div className="pb-0 pt-3 px-4 account-information">
                                                        <div className="d-flex align-items-center justify-content-between">
                                                            <div>
                                                                <h6>Account Information</h6>
                                                            </div>
                                                            <div className="d-flex">
                                                                <ResetButton 
                                                                    title="Reset Customer Pin"
                                                                    className="mr-3"
                                                                    onClick={() => setResetCustomerPin(true)}
                                                                />
                                                                <UpdateDropDown />
                                                            </div>
                                                        </div>
                                                        <hr />
                                                        <Row>
                                                            <Col md={4} className="border-right pr-0">
                                                                <Nav tabs className="account-tab pt-4">
                                                                    <NavItem>
                                                                        <NavLink className="active" href="#account-info">
                                                                            Account Information
                                                                        </NavLink>
                                                                    </NavItem>
                                                                    <NavItem>
                                                                        <NavLink href="#nextofkin-info">
                                                                            Next of Kin Information
                                                                        </NavLink>
                                                                    </NavItem>
                                                                    <NavItem>
                                                                        <NavLink href="#kyc-doc">
                                                                            KYC Document
                                                                        </NavLink>
                                                                    </NavItem>
                                                                </Nav>
                                                            </Col>
                                                            <Col md={8}>
                                                                <TabContent activeTab={activeTab} className="pt-4 pl-3 account-content">
                                                                    <TabPane tabId="1">
                                                                        <div>
                                                                            <div id="account-info">
                                                                                <div className="account-wrapper">
                                                                                    <p className="mb-0">Account Information</p>
                                                                                </div>
                                                                                <div>
                                                                                    {accountInfo.map((data) => (
                                                                                        <div className="d-flex justify-content-between" key={data.id}>
                                                                                            <div>
                                                                                                <p className="my-3">{data.title}</p>   
                                                                                            </div>
                                                                                            <div>
                                                                                                <p className="my-3">{data.value}</p>   
                                                                                            </div>
                                                                                        </div> 
                                                                                    ))}
                                                                                </div>
                                                                            </div>
                                                                            <div className="mt-4" id="nextofkin-info">
                                                                                <div className="account-wrapper">
                                                                                    <p className="mb-0">Next of Kin</p>
                                                                                </div>
                                                                                <div>
                                                                                    {nextOfKinInfo.map((data) => (
                                                                                        <div className="d-flex justify-content-between" key={data.id}>
                                                                                            <div>
                                                                                                <p className="my-3">{data.title}</p>   
                                                                                            </div>
                                                                                            <div>
                                                                                                <p className="my-3">{data.value}</p>   
                                                                                            </div>
                                                                                        </div> 
                                                                                    ))}
                                                                                </div>
                                                                            </div>
                                                                            <div className="mt-4 pb-3" id="kyc-doc">
                                                                                <div className="account-wrapper">
                                                                                    <p className="mb-0">KYC Data</p>
                                                                                </div>
                                                                                <div>
                                                                                    <p className="text-left">No Document Provided yet</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </TabPane>
                                                                </TabContent>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </TabPane>
                                        <TabPane tabId="2">
                                            <TransactionHistoryTable setShowTransactionDetails={setShowTransactionDetails} />
                                        </TabPane>
                                        <TabPane tabId="3">
                                            <CustomerWalletTab />
                                        </TabPane>
                                        <TabPane tabId="4">
                                            <ReferralTable />
                                        </TabPane>
                                        <TabPane tabId="5">
                                            <IssueLogTable />
                                        </TabPane>
                                    </TabContent>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </>
            ) : (
                <TransactionHistoryDetails />
            )}

            <ResetPinDialog 
                title="customer"
                isOpen={resetCustomerPin}
                toggle={toggleResetCustomerPinModal}
            />

            <ReasonForSuspendedAccountDialog
                isOpen={suspendedAccount}
                toggle={toggleSuspendedAccountModal}
            />

        </>
    );
}
