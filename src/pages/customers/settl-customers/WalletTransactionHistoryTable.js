import React, { useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import WalletTransactionHistoryTableHeader from "./WalletTransactionHistoryTableHeader";
import Loader from "../../../components/Loader";

const tableData = [
    {
        id: 1,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Refund"
    },
    {
        id: 2,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Commission"
    },
    {
        id: 3,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Refund"
    },
    {
        id: 4,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Refund"
    },
    {
        id: 5,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Commission"
    },
    {
        id: 6,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Commission"
    },
    {
        id: 7,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Commission"
    },
    {
        id: 8,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Commission"
    },
    {
        id: 9,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Commission"
    },
    {
        id: 10,
        date: "10-01-2020",
        walletType: "Primary",
        transactionRef: "SETT-TRA...",
        transactionType: "Debit",
        amount: "70,000.00",
        paymentSource: "Commission"
    }
]

const tableColumns = [
    {
      dataField: "date",
      text: "Date"
    },
    {
        dataField: "walletType",
        text: "Wallet Type"
    },
    {
        dataField: "transactionRef",
        text: "Tranx Ref."
    },
    {
        dataField: "transactionType",
        text: "Tranx Type"
    },
    {
        dataField: "amount",
        text: "Amount",
        classes: "text-right",
        headerClasses: "text-right"
    },
    {
      dataField: "paymentSource",
      text: "Payment Source"
    }
];

export default function WalletTransactionHistoryTable() {
    const [isfiltered, setIsFiltered] = useState(false);
    const [data, setData] = useState(tableData);

    const Filter = (data) => {
        setData("");
        const check = data.source.length > 0
    
        setTimeout(() => {
          const filtered = tableData.filter((product) => {
            if (check) {
              setIsFiltered(true);
              return (
                data.source.includes(product.paymentSource.toLowerCase())
              );
            } else {
              setIsFiltered(false);
              return product;
            }
          });
          setData(filtered);
        }, 500);
    };

    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total">
            Showing { from } to { to } of { size } Results
        </span>
    );

    const rowEvents = {
        onClick: (e, row, rowIndex) => {
            console.log(rowIndex);
        }
    }


    return(
        <>
            <div className="admin-body">
                <div className="all-admins">
                    <h6>Transaction History</h6>
                    <div className="admin-table">
                        <ToolkitProvider
                            responsive
                            keyField="id"
                            data={data}
                            columns={tableColumns}
                            exportCSV = {{
                                fileName: "wallet transaction history data.csv",
                            }}
                            search
                        >
                            {
                                props => (
                                <div>
                                    <WalletTransactionHistoryTableHeader 
                                        csvData={props.csvProps}
                                        filter={Filter}
                                        filtered={isfiltered}
                                        dataLength={data.length}
                                        search={props.searchProps}  
                                    />
                                    {!data ? (
                                        <Loader />
                                    ) :(
                                        <BootstrapTable
                                            bootstrap4
                                            bordered={false}
                                            wrapperClasses="table-responsive"
                                            rowEvents={rowEvents}
                                            pagination={paginationFactory({
                                                sizePerPage: 9,
                                                hideSizePerPage: true,
                                                showTotal: true,
                                                paginationTotalRenderer: customTotal,
                                            })}
                                            { ...props.baseProps }
                                        />
                                    )}
                                </div>)
                            }
                        </ToolkitProvider>
                    </div>
                </div>
            </div>
        </>
    );
}
