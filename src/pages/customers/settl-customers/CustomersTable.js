import React, { useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import paginationFactory from "react-bootstrap-table2-paginator";
import Loader from "../../../components/Loader";
import { Badge } from "reactstrap";
import CustomerTableHeader from "./CustomerTableHeader";

const tableData = [
    {
        id: 1,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Active"
    },
    {
        id: 2,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Inactive"
    },
    {
        id: 3,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "New"
    },
    {
        id: 4,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Active"
    },
    {
        id: 5,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Inactive"
    },
    {
        id: 6,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "New"
    },
    {
        id: 7,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Dormant"
    },
    {
        id: 8,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Active"
    },
    {
        id: 9,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Inactive"
    },
    {
        id: 10,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "New"
    },
    {
        id: 11,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Active"
    },
    {
        id: 12,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Inactive"
    },
    {
        id: 13,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "New"
    },
    {
        id: 14,
        name: "Carolyn Harvey",
        phoneNumber: "08056389029",
        customerId: "SETT-27499066",
        walletBalance: "₦70,000.00",
        lastActive: "5-1-2021 10:38 AM",
        status: "Dormant"
    },
]

const tableColumns = [
    {
      dataField: "name",
      text: "Name"
    },
    {
      dataField: "phoneNumber",
      text: "Phone number"
    },
    {
      dataField: "customerId",
      text: "Customer ID"
    },
    {
      dataField: "walletBalance",
      text: "Prim. Wallet Balance",
      classes: "text-right",
      headerClasses: "text-right"
    },
    {
      dataField: "lastActive",
      text: "Last active"
    },
    {
        dataField: "status",
        text: "Status",
        headerClasses: "text-center",
        formatter: (cell, row, rowIndex) => {
            if (cell.toLowerCase() === "inactive") {
                return <Badge color="secondary" className="badge-inactive">{cell}</Badge>
            }
            if (cell.toLowerCase() === "active") {
                return <Badge color="secondary" className="badge-active">{cell}</Badge>
            }
            if (cell.toLowerCase() === "new") {
                return <Badge color="secondary" className="badge-new">{cell}</Badge>
            }
            if (cell.toLowerCase() === "dormant") {
                return <Badge color="secondary" className="badge-dormant">{cell}</Badge>
            }
        }
    }
];

export default function CustomerTable({setShowDetails}) {
    const [isfiltered, setIsFiltered] = useState(false);
    const [data, setData] = useState(tableData);

    const Filter = (data) => {
        setData("");
        const check = data.status.length > 0
    
        setTimeout(() => {
          const filtered = tableData.filter((product) => {
            if (check) {
              setIsFiltered(true);
              return (
                data.status.includes(product.status.toLowerCase())
              );
            } else {
              setIsFiltered(false);
              return product;
            }
          });
          setData(filtered);
        }, 500);
    };
    
    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total">
          Showing { from } to { to } of { size } Results
        </span>
    );

    const rowEvents = {
        onClick: (e, row, rowIndex) => {
            setShowDetails((prev) => !prev);
        }
    }
    
    return(
        <>
            <div className="admin-body">
                <div className="all-admins">
                    <h6 className="table-title">All Settl Customers</h6>
                    <div className="admin-table">
                        <ToolkitProvider
                            responsive
                            keyField="id"
                            data={data}
                            columns={tableColumns}
                            exportCSV={{
                                fileName: "customer data.csv",
                            }}
                            search
                        >
                            {
                                props => (
                                <div>
                                    <CustomerTableHeader 
                                        csvData={props.csvProps}
                                        filter={Filter}
                                        filtered={isfiltered}
                                        dataLength={data.length}
                                        search={props.searchProps}
                                    />
                                    {!data ? (
                                        <Loader />
                                    ) : (
                                        <BootstrapTable
                                            bootstrap4
                                            bordered={false}
                                            wrapperClasses="table-responsive"
                                            rowEvents={rowEvents}
                                            pagination={paginationFactory({
                                                sizePerPage: 9,
                                                hideSizePerPage: true,
                                                showTotal: true,
                                                paginationTotalRenderer: customTotal,
                                            })}
                                            { ...props.baseProps }
                                        />
                                    )}
                                </div>)
                            }
                        </ToolkitProvider>
                    </div>
                </div>
            </div>
        </>
    );
}
