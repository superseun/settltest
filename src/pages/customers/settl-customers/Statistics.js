import React from "react";
import StatisticsCard from "./StatisticsCard";
import { Row, Col } from "reactstrap";

const data = [
    {
        id: 1,
        title: "Total Volume of new User",
        value: "32,400",
        percentage: "(+51%)",
        subtitle: "Analytics for last 30 days"
    },
    {
        id: 2,
        title: "Total Volume of Active Users",
        value: "32,400",
        percentage: "(+51%)",
        subtitle: "Analytics for last 30 days"
    },
    {
        id: 3,
        title: "Total Volume of Inactive Users",
        value: "32,400",
        percentage: "(+51%)",
        subtitle: "Analytics for last 30 days"
    },
    {
        id: 4,
        title: "Total Volume of Dormant Users",
        value: "32,400",
        percentage: "(+51%)",
        subtitle: "Analytics for last 30 days"
    }
]

export default function Statistics(props) {
    return(
        <>
            <div className="container-fluid mb-5">
                <Row className="transaction_analysis_section">
                    {data.map((datum) => (
                        <Col md={3} lg={3} sm={6} xs={6} key={datum.id}>
                            <StatisticsCard
                                title={datum.title}
                                value={datum.value}
                                percentage={datum.percentage}
                                subtitle={datum.subtitle}
                            />
                        </Col>
                    ))}
                </Row>
            </div>
        </>
    );
}
