import React from "react";
import WalletTransactionHistoryTable from "./WalletTransactionHistoryTable";
import WalletDistribution from "./WalletDistribution";
import WalletCard from "./WalletCard";

export default function CustomerWalletTab() {
    return(
        <>
            <WalletCard />
            <WalletDistribution />
            <WalletTransactionHistoryTable />
        </>
    );
}
