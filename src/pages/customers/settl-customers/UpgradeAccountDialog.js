import React, { useState } from "react";
import { ReactComponent as Close } from "../../../BgImages/close.svg";
import { 
    Button,
    Modal, 
    ModalBody,
    ModalHeader,
    Form,
    FormGroup,
    Input,
    Label
} from "reactstrap";

const options = [
    {
        label: "International Passport",
        value: "International Passport"
    },
    {
        label: "National ID",
        value: "National ID"
    },
    {
        label: "Voter’s Card",
        value: "Voter’s Card"
    },
    {
        label: "Driver's License",
        value: "Driver's License"
    }
] 

export default function UpgradeAccountDialog(props) {
    const [idDocument, setIdDocument] = useState("select");

    return(
        <>
            <Modal isOpen={props.isOpen} toggle={props.toggle} className="admin-modal upgrade-modal">
                <ModalHeader>
                    Upgrade Customer  Account
                    <Close
                        className="close-modal"
                        onClick={props.toggle}
                    />
                </ModalHeader>
                <ModalBody>
                    <div className="text-center">
                        <p>Please provide the customer passport and a verification document. Each file size shouldn’t exceed 50kb</p>
                    </div>
                    <Form className="px-3 mt-4">
                        <FormGroup>
                            <Label for="customerPassport">Customer Passport</Label>
                            <Input type="file" id="customerPassport" />
                        </FormGroup>
                        <FormGroup className="mt-4">
                            <Input 
                                type="select" 
                                name="idDocument" 
                                id="idDocument"
                                value={idDocument}
                                onChange={(e) => setIdDocument(e.target.value)}
                            >
                                <option value="select" defaultValue disabled>Select Identification document</option>
                                {options.map((option) => (
                                    <option value={option.value} key={option.label}>{option.label}</option>
                                ))}
                            </Input>
                        </FormGroup>
                        {
                            idDocument === "International Passport" ? (
                            <FormGroup className="mt-4">
                                <Label for="internationPassport">International Passport</Label>
                                <Input type="file" id="internationPassport" />
                            </FormGroup>) : ("")
                        }
                        {
                            idDocument === "National ID" ? (
                                <FormGroup className="mt-4">
                                    <Label for="nationalId">National ID</Label>
                                    <Input type="file" id="nationalId" />
                                </FormGroup>
                            ) : ("")
                        }
                        {
                            idDocument === "Voter’s Card" ? (
                                <FormGroup className="mt-4">
                                    <Label for="voterCard">Voter’s Card</Label>
                                    <Input type="file" id="voterCard" />
                                </FormGroup>
                            ) : ("")
                        }
                        {
                            idDocument === "Driver's License" ? (
                                <FormGroup className="mt-4">
                                    <Label for="driverLicense">Driver's License</Label>
                                    <Input type="file" id="driverLicense" />
                                </FormGroup>
                            ) : ("")
                        }
                        
                    </Form>
                    <br/><br/><br/><br/>
                    <hr/>
                    <div className="d-flex justify-content-between">
                        <Button color="info" className="btn-cancel" onClick={props.toggle}>Cancel</Button>
                        <Button color="primary" className="btn-add">Upgrade Account</Button>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}
