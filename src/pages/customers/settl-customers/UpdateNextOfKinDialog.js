import React, { useState } from "react";
import { ReactComponent as Close } from "../../../BgImages/close.svg";
import { 
    Button,
    Modal, 
    ModalBody,
    ModalHeader,
    Form,
    FormGroup,
    Label
} from "reactstrap";
import InputBox from "../../../components/InputBox";

export default function UpdateNextOfKinDialog(props) {
    const [fullName, setFullName] = useState("Assurance");
    const [emailAddress, setEmailAddress] = useState("Assurance.uwangue@thebulb.africa");
    const [phoneNumber, setPhoneNumber] = useState("09029244116");

    return(
        <>
            <Modal isOpen={props.isOpen} toggle={props.toggle} className="admin-modal suspended-modal">
                <ModalHeader>
                    Update Next of Kin Information
                    <Close
                        className="close-modal"
                        onClick={props.toggle}
                    />
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="fullName" 
                                id="fullName"
                                value={fullName}
                                onChange={(e) => setFullName(e.target.value)}
                            />
                            <Label for="fulltName">Full Name</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="emailAddress" 
                                id="emailAddress"
                                value={emailAddress}
                                onChange={(e) => setEmailAddress(e.target.value)}
                            />
                            <Label for="emailAddress">Email address</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="phoneNumber" 
                                id="phoneNumber"
                                value={phoneNumber}
                                onChange={(e) => setPhoneNumber(e.target.value)}
                            />
                            <Label for="adminFullName">Phone number</Label>
                        </FormGroup>
                    </Form>
                    <br/><br/><br/><br/>
                    <hr/>
                    <div className="d-flex justify-content-between">
                        <Button color="info" className="btn-cancel" onClick={props.toggle}>Cancel</Button>
                        <Button color="primary" className="btn-add">Update</Button>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}
