import React, { useState } from "react";
import { Badge } from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import paginationFactory from "react-bootstrap-table2-paginator";
import Loader from "../../../components/Loader";
import ReferralTableHeader from "./ReferralTableHeader";

const tableData = [
    {
        id: 1,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Paid",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 2,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Pending",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 3,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Paid",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 4,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Pending",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 5,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Paid",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 6,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Paid",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 7,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Pending",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 8,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Paid",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 9,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Paid",
        bonus: "N200",
        referralStatus: "Active"
    },
    {
        id: 10,
        referralName: "Assurance Uwangue",
        date: "05-5-2021  10:38 AM",
        bonusStatus: "Paid",
        bonus: "N200",
        referralStatus: "Inactive"
    }
]

const tableColumns = [
    {
      dataField: "referralName",
      text: "Referral Name"
    },
    {
      dataField: "date",
      text: "Date"
    },
    {
        dataField: "bonusStatus",
        text: "Bonus Status",
        headerClasses: "text-center",
        formatter: (cell, row, rowIndex) => {
            if (cell.toLowerCase() === "paid") {
                return <Badge color="secondary" className="badge-active">{cell}</Badge>
            }
            if (cell.toLowerCase() === "pending") {
                return <Badge color="secondary" className="badge-pending">{cell}</Badge>
            }
        }
    },
    {
      dataField: "bonus",
      text: "Bonus",
    },
    {
        dataField: "referralStatus",
        text: "Referral Status",
        headerClasses: "text-center",
        formatter: (cell, row, rowIndex) => {
            if (cell.toLowerCase() === "inactive") {
                return <Badge color="secondary" className="badge-inactive">{cell}</Badge>
            }
            if (cell.toLowerCase() === "active") {
                return <Badge color="secondary" className="badge-active">{cell}</Badge>
            }
        }
    }
];

export default function ReferralTable() {
    const [isfiltered, setIsFiltered] = useState(false);
    const [data, setData] = useState(tableData);

    const Filter = (data) => {
        setData("");
        const check = data.status.length > 0
    
        setTimeout(() => {
          const filtered = tableData.filter((product) => {
            if (check) {
              setIsFiltered(true);
              return (
                data.status.includes(product.referralStatus.toLowerCase())
              );
            } else {
              setIsFiltered(false);
              return product;
            }
          });
          setData(filtered);
        }, 500);
    };

    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total">
          Showing { from } to { to } of { size } Results
        </span>
    );

    return(
        <>
            <div className="admin-body">
                <div className="all-admins">
                    <h6>Referred Customers</h6>
                    <ToolkitProvider
                        responsive
                        keyField="id"
                        data={data}
                        columns={tableColumns}
                        search
                    >
                        {
                            props => (
                                <div>
                                    <ReferralTableHeader
                                        filter={Filter}
                                        filtered={isfiltered}
                                        dataLength={data.length}
                                        search={props.searchProps}
                                    />
                                    {!data ? (
                                        <Loader />
                                    ) : (
                                        <div className="admin-table">
                                            <BootstrapTable
                                                bootstrap4
                                                bordered={false}
                                                wrapperClasses="table-responsive"
                                                pagination={paginationFactory({
                                                    sizePerPage: 9,
                                                    hideSizePerPage: true,
                                                    showTotal: true,
                                                    paginationTotalRenderer: customTotal,
                                                })}
                                                { ...props.baseProps }
                                            />
                                        </div>
                                    )} 
                                </div>
                            )
                        }
                    </ToolkitProvider>
                </div>
            </div>
        </>
    );
}
