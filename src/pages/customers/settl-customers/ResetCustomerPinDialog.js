import React, { useState } from "react";
import { ReactComponent as Close } from "../../../BgImages/close.svg";
import { 
    Button,
    Modal, 
    ModalBody,
    ModalHeader,
    Form,
    FormGroup,
    Label 
} from "reactstrap";
import InputBox from "../../../components/InputBox";

export default function ResetCustomerPinDialog(props) {
    const [adminFullName, setAdminFullName] = useState("");
    
    return(
        <>
            <Modal isOpen={props.isOpen} toggle={props.toggle} className="admin-modal freeze-modal">
                <ModalHeader>
                    Reset Customer Pin
                    <Close
                        className="close-modal"
                        onClick={props.toggle}
                    />
                </ModalHeader>
                <ModalBody>
                    <div>
                        <div className="text-center">
                            <p>Please confirm this is the right agent account before initiating this process</p>
                        </div>
                        <Form>
                            <FormGroup>
                                <InputBox
                                    type="text"
                                    name="adminFullName" 
                                    id="adminFullName"
                                    value={adminFullName}
                                    onChange={(e) => setAdminFullName(e.target.value)}
                                />
                                <Label for="adminFullName">Admin Full Name</Label>
                            </FormGroup>
                        </Form>
                        <div className="w-100 mt-5">
                            <Button color="primary" className="btn-add w-100 py-3">Continue</Button>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}
