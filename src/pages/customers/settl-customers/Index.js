import React, { useState } from "react";
import CustomerDetails from "./CustomerDetails";
import CustomerTable from "./CustomersTable";
import Header from "./Header";
import Statistics from "./Statistics";

export default function SettlCustomers() {
    const [showDetails, setShowDetails] = useState(false);

    return(
        <>
            <div className="customers">
                {!showDetails ? ( 
                    <>
                        <Header title="Customer Data" />
                        <Statistics /> 
                        <CustomerTable setShowDetails={setShowDetails}  />
                    </>
                ) : (
                    <CustomerDetails setShowDetails={setShowDetails} />
                )}
            </div>
        </>
    );
}
