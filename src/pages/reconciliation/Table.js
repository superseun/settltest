import React from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from "react-bootstrap-table2-toolkit";

const data = [
  {
    date: "05-5-2021 ",
    type: "Bill Payment",
    duration: "05-5-20 - 06-05-20",
    vendor: "FCMB",
    status: "Reconciled",
  },
  {
    date: "05-5-2021 ",
    type: "Bill Payment",
    duration: "05-5-20 - 06-05-20",
    vendor: "Paystack",
    status: "Draft",
  },
];

const Table = ({ setShowForm, setDetail }) => {
  const columns = [
    {
      dataField: "date",
      text: "Date Created",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
      },
    },
    {
      dataField: "duration",
      text: "Duration",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
      },
    },
    {
      dataField: "vendor",
      text: "Vendor/Bank",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
      },
    },
    {
      dataField: "status",
      text: "Status",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        width: "150px",
        textAlign: "center",
      },
      style: () => {
        return {
          margin: "auto 0",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: "20px 0",
        };
      },
      formatter: (cell, row, rowIndex) => {
        if (cell.toLowerCase() === "draft") {
          return <span className="status pending">{cell}</span>;
        }
        if (cell.toLowerCase() === "reconciled") {
          return <span className="status success">{cell}</span>;
        }
      },
    },
  ];
  const customTotal = (from, to, size) => (
    <span
      className="react-bootstrap-table-pagination-total"
      style={{
        padding: "1rem",
      }}
    >
      Showing {from} to {to} of {size} Results
    </span>
  );
  const rowStyle = {
    border: "none",
    cursor: "pointer",
  };
  const rowEvents = {
    onClick: () => {
      setDetail({
        service: "Bill Payment",
        bank: "FirstBank",
        duration: "Last 7 Days",
      });
      setShowForm(true);
    },
  };
  return (
    <ToolkitProvider keyField="date" data={data} columns={columns}>
      {(props) => (
        <div className="table">
          <p
            className="table_title"
            style={{
              border: "none",
              paddingBottom: "0",
            }}
          >
            {" "}
            Reconciliation Log
          </p>
          <BootstrapTable
            {...props.baseProps}
            wrapperClasses="reconciliation-table"
            bordered={false}
            rowStyle={rowStyle}
            rowEvents={rowEvents}
            pagination={paginationFactory({
              sizePerPage: 9,
              hideSizePerPage: true,
              showTotal: true,
              paginationTotalRenderer: customTotal,
            })}
          />
        </div>
      )}
    </ToolkitProvider>
  );
};
export default Table;
