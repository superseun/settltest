import React, { useState } from "react";
import { 
    Button,
    Col,
    Form,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalHeader,
    ModalFooter,
    Row,
} from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import InputBox from "../../components/InputBox";
import ButtonBox from "../../components/ButtonBox";
import { Multiselect } from 'multiselect-react-dropdown';
import { Edit2, Trash } from "react-feather";
import securityIcon from "./../../assets/img/icons/security.svg";
import online from "./../../assets/img/icons/online.svg";

const tableData = [
    {
        id: 1,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 2,
        name: "Carolyn Harvey",
        position: "Frontend Engineer",
        type: "Admin",
        tier: 3,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 3,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 4,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 5,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 6,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 7,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 8,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 9,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 10,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 11,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
    {
        id: 12,
        name: "Carolyn Harvey",
        position: "Engineering",
        type: "Supper Admin",
        tier: 2,
        actions: <div>
            <Edit2 className="align-middle edit mr-3" size={18} />
            <Trash className="align-middle delete" size={18} />
        </div>
    },
]

const tableColumns = [
    {
      dataField: "name",
      text: "Name",
      sort: false
    },
    {
      dataField: "position",
      text: "Position",
      sort: false
    },
    {
      dataField: "type",
      text: "Type",
      sort: false
    },
    {
      dataField: "tier",
      text: "Tier",
      sort: false
    },
    {
      dataField: "actions",
      text: "Action",
      sort: false
    }
];

const detailsData = [
    { title: "First Name", value: "Assurance"},
    { title: "Last Name", value: "Uwangue"},
    { title: "Email", value: "Assurance.uwangue@thebulb.africa"},
    { title: "Phone Number", value: "09029244116"},
    { title: "Position", value: "Engineering"},
    { title: "Level", value: "tier 3"},
    { title: "Status", value: <div className="d-flex align-items-center"><img src={online} className="mr-2" alt=""/> Active</div> },
]

export default function AdminTable() {
    const [adminDetailsModal, setAdminDetailsModal] = useState(false);
    const [deactivateModal, setDeactivateModal] = useState(false);
    const [updateModal, setUpdateModal] = useState(false);
    const [adminFullName, setAdminFullName] = useState("Assurance Uwangu");
    const [adminEmailAddress, setAdminEmailAddress] = useState("Assurance.uwangue@thebulb.africa");
    const [adminPhoneNumber, setAdminPhoneNumber] = useState("09029244116");
    const [options] = useState([{name: 'Admin', id: 1},{name: 'Super Admin', id: 2}]);
    
    const toggleDeactivateModal = () => setDeactivateModal(!deactivateModal);
    const toggleUpdateModal = () => setUpdateModal(!updateModal);
    const toggleDetailsModal = () => setAdminDetailsModal(!adminDetailsModal);

    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total">
            Showing { from } to { to } of { size } Results
        </span>
    );

    const rowEvents = {
        onClick: (e, row, rowIndex) => {
          toggleDetailsModal()
        }
    }

    return(
        <>
            <div className="admin-table mt-4">
                <BootstrapTable
                    keyField="id"
                    data={tableData}
                    columns={tableColumns}
                    rowEvents={rowEvents}
                    bootstrap4
                    bordered={false}
                    pagination={paginationFactory({
                        sizePerPage: 9,
                        hideSizePerPage: true,
                        showTotal: true,
                        paginationTotalRenderer: customTotal,
                    })}
                />
            </div>

            <Modal isOpen={adminDetailsModal} toggle={toggleDetailsModal} className="details-modal">
                <ModalBody>
                    <div>
                        <h6 className="p-3 mb-3">Admin Details</h6>
                        {detailsData.map((data, index) => {
                            return(
                                <div className="details-wrapper d-flex justify-content-between px-3 mb-3" key={index}>
                                    <div>
                                        <p className="details-title">{data.title}</p>
                                    </div>
                                    <div>
                                        <p className="details-value">{data.value}</p>
                                    </div>
                                </div>
                            );  
                        })}
                    </div>
                    <Row className="px-3">
                        <Col md={6}>
                            <div>
                                <Button color="info" className="btn-cancel" onClick={toggleUpdateModal}>Update Admin</Button>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div>
                                <Button color="danger" className="btn-delete w-100" onClick={toggleDeactivateModal}>Deactivate Admin</Button>
                            </div>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>

            {/* Update modal  */}
            <Modal isOpen={updateModal} toggle={toggleUpdateModal} className="admin-modal update-modal">
                <ModalHeader toggle={toggleUpdateModal}>Update Admin details</ModalHeader>
                <ModalBody className="pt-3">      
                    <Form className="mt-3">
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="adminFullName" 
                                id="adminFullName"
                                value={adminFullName}
                                onChange={(e) => setAdminFullName(e.target.value)}
                            />
                            <Label for="adminFullName">Full Name</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="email"
                                name="adminEmailAddress" 
                                id="adminEmailAddress"
                                value={adminEmailAddress}
                                onChange={(e) => setAdminEmailAddress(e.target.value)}
                            />
                            <Label for="adminEmailAddress">Email Address</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="adminPhoneNumber" 
                                id="adminPhoneNumber"
                                value={adminPhoneNumber}
                                onChange={(e) => setAdminPhoneNumber(e.target.value)}
                            />
                            <Label for="adminPhoneNumber">Phone number</Label>
                        </FormGroup>
                        <FormGroup>
                            <Multiselect className="forget_pass_select"
                                options={options} // Options to display in the dropdown
                                showCheckbox={true}
                                showArrow={true}
                                style={ { border: "3px solid red", "border-bottom": "1px solid blue", "border-radius": "0px" } }
                                selectedValues="" // Preselected value to persist in dropdown
                                onSelect="" // Function will trigger on select event
                                onRemove="" // Function will trigger on remove event
                                displayValue="name" // Property name to display in the dropdown options
                                placeholder="Admin Role"
                                id="css_custom"
                            />
                        </FormGroup>
                        <FormGroup>
                            <InputBox 
                                type="select" 
                                name="position" 
                                id="position"
                            >
                                <option value="backend-engineer">Backend Engineer</option>
                                <option value="frontend-engineer">Frontend Engineer</option>
                            </InputBox>
                        </FormGroup>
                        <hr/>
                        <div className="d-flex justify-content-between">
                            <Button color="info" className="btn-cancel" onClick={toggleUpdateModal}>Cancel</Button>
                            <ButtonBox onClick="" name="Update Details" />
                        </div>
                    </Form>
                </ModalBody>
            </Modal>

            {/* Deactivate Modal */}
            <Modal isOpen={deactivateModal} toggle={toggleDeactivateModal} className="admin-modal deactivate-modal">
                <ModalHeader toggle={toggleDeactivateModal}>Deactivation Confirmation</ModalHeader>
                <ModalBody className="text-center pt-4 px-5 mx-2">
                    <img src={securityIcon} alt="an icon"/> 
                    <p className="pt-3">Deactivating this admin user means they willl no longer be able to view their Dashboard. Are you sure you want to continue?</p>
                </ModalBody>
                <ModalFooter>
                    <Button color="info" className="btn-cancel" onClick={toggleDeactivateModal}>Cancel</Button>
                    <Button color="danger" className="btn-delete">Yes, Delete</Button>{' '}
                </ModalFooter>
            </Modal>
        </>
    );
}