import React, { useState } from "react";
import {
    Button,
    Col,
    Dropdown,
    DropdownMenu,
    DropdownToggle,
    Form,
    FormGroup,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Pagination,
    PaginationItem,
    PaginationLink
} from "reactstrap";
import { Search } from "react-feather";
import Grid from "./../../assets/img/icons/grid.svg";
import GridActive from "./../../assets/img/icons/grid-active.svg";
import List from "./../../assets/img/icons/list.svg";
import ListActive from "./../../assets/img/icons/list-active.svg";
import AdminCard from "./AdminCard";
import AdminTable from "./AdminTable";
import AddNewAdmin from "./AddAdminModal";

const data = [
    { 
        id: 1, 
        name: "Mark Kon",
        email: "Mark.kon@thebulb.africa",
        status: "Pending" 
    },
    { 
        id: 2, 
        name: "Mark Kon",
        email: "Mark.kon@thebulb.africa",
        status: "Super Admin" 
    },
    { 
        id: 3, 
        name: "Mark Kon",
        email: "Mark.kon@thebulb.africa",
        status: "Admin"  
    },
    { 
        id: 4, 
        name: "Mark Kon",
        email: "Mark.kon@thebulb.africa",
        status: "Admin"  
    },
    { 
        id: 5, 
        name: "Mark Kon",
        email: "Mark.kon@thebulb.africa",
        status: "Super Admin" 
    },
    { 
        id: 6, 
        name: "Mark Kon",
        email: "Mark.kon@thebulb.africa",
        status: "Admin" 
    },
    { 
        id: 7, 
        name: "Mark Kon",
        email: "Mark.kon@thebulb.africa",
        status: "Admin" 
    },
    { 
        id: 8, 
        name: "Mark Kon",
        email: "Mark.kon@thebulb.africa",
        status: "Admin" 
    },
];

export default function AdminManagement() {
    const [viewType, setViewType] = useState("list");
    const [dropdownOpen, setDropdownOpen] = useState(false);

    const toggleFilter = () => setDropdownOpen(prevState => !prevState);

    return(
        <>
           <section className="admin-management">
                <div className="admin-header">
                    <Row className="align-items-center">
                        <Col md="7">
                            <Form className="admin-form inline">
                                <FormGroup check className="pr-3 viewtype">
                                    <Label check>
                                        <Input 
                                            type="radio" 
                                            name="viewType"
                                            checked={viewType === "list"}
                                            value="list"
                                            onChange={(e) => setViewType(e.target.value)}
                                        />{' '}
                                        {
                                            viewType === "list" 
                                            ? <img src={ListActive} alt="an icon"/>
                                            : <img src={List} alt="an icon"/>
                                        }
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="pr-3 py-2 mr-3 viewtype">
                                    <Label check>
                                        <Input 
                                            type="radio" 
                                            name="viewType" 
                                            value="grid"
                                            onChange={(e) => setViewType(e.target.value)}
                                        />{' '}
                                        {
                                            viewType === "grid" 
                                            ? <img src={GridActive} alt="an icon"/>
                                            : <img src={Grid} alt="an icon"/>
                                        }
                                    </Label>
                                </FormGroup>
                                <Dropdown isOpen={dropdownOpen} toggle={toggleFilter} className="mr-3 btn-filter">
                                    <DropdownToggle caret>
                                        Filter
                                    </DropdownToggle>
                                    <DropdownMenu className="filter-dropdown-menu">
                                        <div className="form-block">
                                            <FormGroup>
                                                <label>Status</label>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" />
                                                        Active
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check className="mt-2">
                                                        <Input type="checkbox" />
                                                        Deactivated
                                                    </Label>
                                                </FormGroup>
                                            </FormGroup>
                                            <FormGroup className="mt-3">
                                                <Label for="exampleSelect">Admin type</Label>
                                                <Input type="select" name="select" id="exampleSelect" placeholder="Select">
                                                    <option selected disabled>Select</option>
                                                    <option value="super admin">Super Admin</option>
                                                    <option value="admin">Admin</option>
                                                    <option>3</option>
                                                </Input>
                                            </FormGroup>
                                            <div className="d-flex justify-content-between mt-5">
                                                <div>
                                                    <Button color="info" className="btn-cancel" onClick="">Clear</Button>
                                                </div>
                                                <div>
                                                    <Button color="primary" className="btn-add" onClick="">Filter</Button>
                                                </div>
                                            </div>
                                        </div>
                                    </DropdownMenu>
                                </Dropdown>
                                <InputGroup className="search-group">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText><Search/></InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="Search" />
                                </InputGroup>
                            </Form>
                        </Col>
                        <Col md="5">
                            <AddNewAdmin />
                        </Col>
                    </Row>
                </div>
                <div className="admin-body">
                    <div className="all-admins pb-2">
                        <h6>All Admins</h6>
                        {
                            viewType === "list" ? <AdminTable/> 
                            : 
                            <div style={{ padding: "0 1rem" }}>
                                <Row> 
                                    {data.map((admin) => (
                                        <Col lg={3} md={3} sm={6} key={admin.id}>
                                            <AdminCard
                                                name={admin.name}
                                                email={admin.email}
                                                status={admin.status}
                                            /> 
                                        </Col>
                                    ))}
                                </Row>
                                <Row>
                                    <Col md={4}>
                                        <div>
                                            <p style={{color: "#637381"}}>Showing 9 of 290 results</p>
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                        <Pagination aria-label="Page navigation example">
                                            <PaginationItem>
                                                <PaginationLink previous href="#" />
                                            </PaginationItem>
                                            <PaginationItem>
                                                <PaginationLink href="#">
                                                1
                                                </PaginationLink>
                                            </PaginationItem>
                                            <PaginationItem>
                                                <PaginationLink href="#">
                                                2
                                                </PaginationLink>
                                            </PaginationItem>
                                            <PaginationItem>
                                                <PaginationLink href="#">
                                                3
                                                </PaginationLink>
                                            </PaginationItem>
                                            <PaginationItem>
                                                <PaginationLink next href="#" />
                                            </PaginationItem>
                                        </Pagination>
                                    </Col>
                                </Row>
                            </div>
                        }
                    </div>
                </div>
           </section>
        </>
    );
}