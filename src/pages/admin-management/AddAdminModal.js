import React, { useState } from "react";
import {
    Button,
    Form,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalHeader,
} from "reactstrap";
import { Plus } from "react-feather";
import InputBox from "../../components/InputBox";
import ButtonBox from "../../components/ButtonBox";
import { Multiselect } from 'multiselect-react-dropdown';
import checkIcon from "./../../assets/img/icons/check.svg";

const options = [
    { name: 'Admin' },
    { name: 'Super Admin' }, 
    { name: 'Support' }
]

export default function AddNewAdmin() {
    const [addAdminModal, setAddAdminModal] = useState(false);
    const [completeModal, setCompleteModal] = useState(false);
    const [employeeName, setEmployeeName] = useState("");
    const [employeeEmailAddress, setEmployeeEmailAddress] = useState("");
    const [employeePhoneNumber, setEmployeePhoneNumber] = useState("");
    const [selectedValues, setSelectedValues] = useState("");
    // const [adminType, setAdminType] = useState("");

    const toggleModal = () => setAddAdminModal(!addAdminModal);
    const toggleCompleteModal = () => setCompleteModal(!completeModal);
    
    const handleClick = () => {
        setAddAdminModal(false);
        setCompleteModal(true);
    }    

    return(
        <>
            <div className="text-right">
                <Button color="primary" className="btn-add" onClick={toggleModal}>
                    <Plus />New Admin
                </Button>
            </div>
            <Modal isOpen={addAdminModal} toggle={toggleModal} className="admin-modal">
                <ModalHeader toggle={toggleModal}>Create New Admin</ModalHeader>
                <ModalBody>
                    <Form className="new-admin-form">
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="employeeName" 
                                id="employeeName"
                                value={employeeName}
                                onChange={(e) => setEmployeeName(e.target.value)}
                            />
                            <Label for="employeeName">Employee Name</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="email"
                                name="employeeEmailAddress" 
                                id="employeeEmailAddress"
                                value={employeeEmailAddress}
                                onChange={(e) => setEmployeeEmailAddress(e.target.value)}
                            />
                            <Label for="employeeEmailAddress">Email Address</Label>
                        </FormGroup>
                        <FormGroup>
                            <InputBox
                                type="text"
                                name="employeePhoneNumber" 
                                id="employeePhoneNumber"
                                value={employeePhoneNumber}
                                onChange={(e) => setEmployeePhoneNumber(e.target.value)}
                            />
                            <Label for="employeePhoneNumber">Phone number</Label>
                        </FormGroup>
                        <Multiselect 
                            className="forget_pass_select"
                            options={options}
                            showCheckbox={true}
                            showArrow={true}
                            // style={ { border: "3px solid red", "border-bottom": "1px solid blue", "border-radius": "0px" } }
                            placeholder={selectedValues === "" ? "Admin Role" : selectedValues}
                            displayValue="name"
                            onSelect={(e) => setSelectedValues(e[0].name)}
                            singleSelect={true}
                        />
                        <hr/>
                        <ButtonBox onClick={handleClick} name="Create Admin" />
                    </Form>
                </ModalBody>
            </Modal>
            <Modal isOpen={completeModal} toggle={toggleCompleteModal} className="admin-modal complete-modal">
                <ModalHeader toggle={toggleCompleteModal}></ModalHeader>
                <ModalBody>
                    <div className="d-flex">
                        <div className="mr-3">
                            <img src={checkIcon} alt="lock icon"/><br/>
                        </div>
                        <div>
                            <h4>Admin Created</h4>
                            <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur</p>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}