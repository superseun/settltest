import React from "react";
import TransactionCard from "./TransactionCard";
import { one, two, three, four, five } from "../../../assets/img/dashboard";

const Statistics = ({ index }) => {
  const transactions = [
    {
      name: ["Total Transaction Volume", "Total Transaction Volume"],
      total_amount: "79,800",
      percentage: "+54",
      icon: one,
    },
    {
      name: ["Total Transaction Value", "Total Transaction Volume"],
      total_amount: "600,400",
      percentage: "+54",
      icon: two,
    },
    {
      name: ["Total Settl Customers", "Total Settl Agents"],
      total_amount: "32,400",
      percentage: "+54",
      icon: three,
    },
    {
      name: ["Active Customers", "Active Agents"],
      total_amount: "12,400",
      percentage: "+54",
      icon: four,
    },
    {
      name: ["Inactive Customers", "Inactive Agents"],
      total_amount: "3,200",
      percentage: "-10",
      icon: one,
    },
    {
      name: ["New Customers", "New Agents"],
      total_amount: "2,400",
      percentage: "+54",
      icon: five,
    },
  ];
  return (
    <div className="transaction_body">
      {transactions.map((transaction, i) => (
        <TransactionCard
          key={i}
          name={transaction.name[index]}
          total_amount={transaction.total_amount}
          percentage={transaction.percentage}
          icon={transaction.icon}
        />
      ))}
    </div>
  );
};

export default Statistics;
