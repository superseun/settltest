import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import { TabContent, TabPane } from "reactstrap";

import BarChart from "./BarChart";

import Header from "./Header";
import AppDownloads from "./AppDownloads";
import Statistics from "./Statistics";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Calendar } from "react-feather";

const Default = () => {
  const [activeTab, setActiveTab] = useState(0);
  const tabs = ["Settl  Customers", "Settl Agents"];
  const chartTitles = [
    { bar: "Customer Transactions", pie: "App Downloads" },
    { bar: "Agent & Super Agent Transactions", pie: "Agent App Downloads" },
  ];
  return (
    <>
      <div className="bg_absolute">
        <Header />
        <div className="nav">
          {tabs.map((tab, i) => (
            <div
              onClick={() => setActiveTab(i)}
              className={`${activeTab === i ? "active_nav" : ""}`}
            >
              <p>{tab}</p>
            </div>
          ))}
        </div>
      </div>
      <div
        style={{
          marginTop: "8rem ",
        }}
      >
        <TabContent activeTab={activeTab}>
          {tabs.map((tab, i) => (
            <TabPane tabId={i} key={i}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginBottom: "10px",
                }}
              >
                <p
                  style={{
                    color: "#304762",
                    fontSize: "20px",
                  }}
                >
                  {tab} Analytics
                </p>
                <UncontrolledDropdown className="d-inline filter-dropdown">
                  <DropdownToggle
                    caret
                    color="light"
                    className="shadow-sm"
                    style={{
                      backgroundColor: "white",
                    }}
                  >
                    <Calendar className="feather align-middle mt-n1" /> Last 30
                    days
                  </DropdownToggle>
                  <DropdownMenu
                    right
                    style={{
                      top: "25px",
                    }}
                  >
                    <DropdownItem>Today</DropdownItem>
                    <DropdownItem>Last 7 Days</DropdownItem>
                    <DropdownItem>Last 90 Days</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>Customize</DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>

              <Statistics index={i} />
              <Row
                style={{
                  marginTop: "2rem",
                }}
              >
                <Col lg="7" className="d-flex">
                  <BarChart title={chartTitles[i].bar} />
                </Col>
                <Col lg="5" className="d-flex">
                  <AppDownloads title={chartTitles[i].pie} />
                </Col>
              </Row>
            </TabPane>
          ))}
        </TabContent>
      </div>
    </>
  );
};
export default Default;
