import React from "react";
import TransactionCard from "../../../components/TransactionCard";

const Statistics = () => {
  const transactions = [
    {
      name: "Total Referral Transaction Volume",
      total_amount: "32,400",
      percentage: "+54",
    },
    {
      name: "Total Referral Transaction Volume",
      total_amount: "32,400",
      percentage: "+54",
    },
    {
      name: "Total value of Referral Bonuses",
      total_amount: "32,400",
      percentage: "+54",
    },
    {
      name: "Total Referrals Links Sent ",
      total_amount: "32,400",
      percentage: "+54",
    },
    {
      name: "Total Number of Registered Referrals",
      total_amount: "32,400",
      percentage: "+54",
    },
    {
      name: "Total Number of Active Referrals",
      total_amount: "32,400",
      percentage: "+54",
    },
  ];
  return (
    <div className="transaction_body">
      {transactions.map((transaction, i) => (
        <TransactionCard
          key={i}
          name={transaction.name}
          total_amount={transaction.total_amount}
          percentage={transaction.percentage}
        />
      ))}
    </div>
  );
};

export default Statistics;
