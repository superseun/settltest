import React from "react";
import Header from "../../../components/Header";
import Statistics from "./Statistic";
import Table from "./Table";
import logo from "../../../assets/img/icons/referralicon.svg";
const Referral = () => (
  <div>
    <Header logo={logo} name="Settl Referral Data" calendar />

    <Statistics />
    <Table />
  </div>
);

export default Referral;
