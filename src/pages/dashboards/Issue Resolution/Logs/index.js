import React, { useState } from "react";
import Table from "../Table";
import Modal from "../Modal";
import Header from "../../../../components/Header";
import Statistics from "../Statistic";
import logo from "../../../../assets/img/icons/issue logo.svg";

const Logs = () => {
  const [show, setShow] = useState(false);
  return (
    <div>
      <Table setShow={setShow} />
      <Modal show={show} setShow={setShow} />
    </div>
  );
};
export default Logs;
