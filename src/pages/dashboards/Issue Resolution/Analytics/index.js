import React from "react";
import Header from "../../../../components/Header";
import logo from "../../../../assets/img/icons/issue logo.svg";
import Statistics from "../Statistic";
import Chart from "./Chart";
import PerformanceChart from "./PerformaceChart";

const Analytics = () => (
  <div>
    <Header logo={logo} name="Issue Analytics" calendar />
    <Statistics />
    <Chart />
    <PerformanceChart />
  </div>
);
export default Analytics;
