import React from "react";
import BootstrapTable from "react-bootstrap-table-next";
import data from "./data";

const PerformanceChart = () => {
  const columns = [
    {
      dataField: "name",
      text: "Customer Sucess Agents",
      headerStyle: {
        color: "#858EBD",
        textTransform: "uppercase",
        fontWeight: "normal",
        border: "none",
        width: "250px",
      },
      formatter: (cell, row, rowIndex) => {
        return (
          <div
            style={{
              display: "flex",
              gap: "1rem",
            }}
          >
            <span
              style={{
                backgroundColor: "#F4F7FF",
                padding: "0.2rem 1rem",
                borderRadius: "8px",
                display: "flex",
                alignItems: "center",
              }}
            >
              {rowIndex + 1}
            </span>
            <p
              style={{
                margin: "auto 0",
                padding: "auto 0",
                fontWeight: "500",
              }}
            >
              {cell}
            </p>
          </div>
        );
      },
    },
    {
      dataField: "resolved issues",
      text: "Resolved issues",
      headerStyle: {
        color: "#858EBD",
        textTransform: "uppercase",
        fontWeight: "normal",
        border: "none",
        textAlign: "center",
      },
      style: () => ({
        textAlign: "center",
        fontWeight: "500",
      }),
    },
    {
      dataField: "progress",
      text: "progress",
      headerStyle: {
        color: "#858EBD",
        textTransform: "uppercase",
        fontWeight: "normal",
        border: "none",
      },
      formatter: (cell, row, rowIndex) => {
        let color;
        if (cell >= 60) {
          color = "#1EB75B";
        } else if (cell < 60 && cell > 30) {
          color = "#EDBF78";
        } else {
          color = "#F50D49";
        }
        return (
          <div
            style={{
              height: "8px",
              backgroundColor: "#F6F4F8",
              borderRadius: "5px",
            }}
          >
            <div
              style={{
                width: `${cell}%`,
                height: "100%",
                backgroundColor: color,
                borderRadius: "5px",
              }}
            ></div>
          </div>
        );
      },
    },
    {
      dataField: "avgResolutionTime",
      text: "Avg. Resolution Time",
      headerStyle: {
        color: "#858EBD",
        textTransform: "uppercase",
        fontWeight: "normal",
        border: "none",
        textAlign: "center",
      },
      style: () => ({
        textAlign: "center",
        fontWeight: "500",
      }),
    },
    {
      dataField: "resolution",
      text: "resolution",
      headerStyle: {
        color: "#858EBD",
        textTransform: "uppercase",
        fontWeight: "normal",
        border: "none",
        width: "250px",
      },
      formatter: (cell, row, rowIndex) => {
        let color;
        if (data[rowIndex].progress >= 60) {
          color = "#1EB75B";
        } else if (
          data[rowIndex].progress < 60 &&
          data[rowIndex].progress > 30
        ) {
          color = "#EDBF78";
        } else {
          color = "#F50D49";
        }
        return (
          <div
            style={{
              display: "flex",
              gap: "1rem",
            }}
          >
            <p
              style={{
                color: "#304762",
                fontWeight: "600",
              }}
            >
              {" "}
              {cell.percentage}%{" "}
            </p>
            <p
              style={{
                color: color,
                fontSize: "12px",
              }}
            >
              ({cell.rate}){" "}
              {data[rowIndex].progress >= 60 ? "\u2191" : "\u2193"}
            </p>
            <p
              style={{
                color: "#798C9C",
                fontSize: "12px",
              }}
            >
              (Versus last 30 days)
            </p>
          </div>
        );
      },
    },
  ];
  const rowStyle = {
    border: "none !important",
  };
  return (
    <div
      style={{
        backgroundColor: "white",
        marginTop: "3rem",
      }}
    >
      <p className="table_title">CS Resolution Performance</p>
      <BootstrapTable
        keyField="id"
        bordered={false}
        data={data}
        columns={columns}
        rowStyle={rowStyle}
      />
    </div>
  );
};
export default PerformanceChart;
