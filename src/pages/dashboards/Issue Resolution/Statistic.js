import React from "react";
import upArrow from "../../../assets/img/icons/uparrow.svg";
import downArrow from "../../../assets/img/icons/downarrow.svg";

const Statistics = () => (
  <div class="container-fluid mb-5">
    <div class="row row-cols-4  transaction_analysis_section">
      <div class="col col_border_right">
        <div className="transaction_volume">
          <h4>Total Issues Reported</h4>
          <p>
            32,400{" "}
            <span>
              +51% <img className="img-fluid" src={upArrow} alt="uparrow" />
            </span>{" "}
          </p>
          <p className="time">Analytics for last 30 days</p>
        </div>
      </div>
      <div class="col col_border_right">
        <div className="transaction_volume">
          <h4>Total Issues Resolved</h4>
          <p>
            32,400{" "}
            <span>
              +51% <img className="img-fluid" src={upArrow} alt="uparrow" />
            </span>{" "}
          </p>
          <p className="time">Analytics for last 30 days</p>
        </div>
      </div>
      <div class="col col_border_right">
        <div className="transaction_volume">
          <h4> Total Pending Issues</h4>
          <p>
            32,400{" "}
            <span>
              +51% <img className="img-fluid" src={upArrow} alt="downarrow" />
            </span>{" "}
          </p>
          <p className="time">Analytics for last 30 days</p>
        </div>
      </div>
      <div class="col">
        <div className="transaction_volume">
          <h4>Issues in Progress</h4>
          <p>
            32,400{" "}
            <span>
              +51% <img className="img-fluid" src={upArrow} alt="uparrow" />
            </span>{" "}
          </p>
          <p className="time">Analytics for last 30 days</p>
        </div>
      </div>
    </div>
  </div>
);

export default Statistics;
