import React, { useState } from "react";
import { ReactComponent as Close } from "../../../BgImages/close.svg";
import { Modal, Button } from "react-bootstrap";
import {
  Input,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import issueimg from "../../../assets/img/photos/issueimage.png";
const IssuesModal = ({ show, setShow }) => {
  const [className, setClassName] = useState("failed");
  const handleChange = (e) => {
    const { value } = e.target;
    setClassName(value);
  };
  const information = [
    {
      "Issue created by": (
        <p
          style={{
            color: " #451B7D",
            borderBottom: "1px solid  #451B7D",
            width: "fit-content",
          }}
        >
          Carolyn Harvey
        </p>
      ),
    },
    {
      "Transaction ID": (
        <p
          style={{
            color: " #451B7D",
            borderBottom: "1px solid  #451B7D",
            width: "fit-content",
          }}
        >
          STTL10933W822
        </p>
      ),
    },
    { "Transaction type": "Transfer" },
    {
      Status: (
        <UncontrolledDropdown className="d-inline filter-dropdown">
          <DropdownToggle
            color="light"
            className="status failed"
            style={{
              border: "none",
            }}
          >
            Unresolved
          </DropdownToggle>
          <DropdownMenu
            right
            style={{
              top: "25px",
              padding: "0.5rem",
            }}
          >
            <DropdownItem
              className=" success"
              style={{
                width: "fit-content",
                margin: "0.5rem",
                borderRadius: "4px",
              }}
            >
              Resolved
            </DropdownItem>
            <DropdownItem
              className=" pending"
              style={{
                width: "fit-content",
                borderRadius: "4px",
              }}
            >
              In-Progress
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        // <Input
        //   type="select"
        //   name="status"
        //   className={`mb-3 status ${className}`}
        //   style={{
        //     width: "fit-content",
        //     border: "none",
        //   }}
        //   onChange={handleChange}
        // >
        //   <option value="failed" className="status failed">
        //     {" "}
        //     Unresolved
        //   </option>
        //   <option value="success" className="status success">
        //     Success
        //   </option>
        //   <option value="pending" className="status pending">
        //     Pending
        //   </option>
        // </Input>
      ),
    },
    {
      Description: (
        <p
          style={{
            opacity: "0.5",
            fontSize: "14px",
          }}
        >
          Lorem ipsum is placeholder text commonly used in the graphic, print,
          and publishing industries for previewing layouts and visual mockups.
        </p>
      ),
    },
    {
      Image: <img src={issueimg} alt="ssueimage" />,
    },
  ];
  return (
    <Modal
      show={show}
      centered
      onHide={() => setShow(false)}
      className="issue-modal"
      size="lg"
      style={{
        color: "#171725",
      }}
    >
      <div
        style={{
          display: "relative",
        }}
      >
        <h3
          style={{
            textAlign: "center",
            borderBottom: "1px solid rgba(231, 231, 237, 0.6)",
            padding: "1.5rem 0",
          }}
        >
          Issue Details
        </h3>
        <Close
          style={{
            position: "absolute",
            top: "20px",
            right: "20px",
            cursor: "pointer",
          }}
          onClick={() => setShow(false)}
        />
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          margin: "0 1.5rem",
          padding: ".6rem 0",
          borderBottom: "1px solid rgba(231, 231, 237, 0.6)",
          fontSize: "13px",
        }}
      >
        <p>
          <span style={{ fontWeight: "600", color: "" }}>Issue ID </span>{" "}
          <span
            style={{
              opacity: "0.5",
            }}
          >
            STTL10933W822
          </span>
        </p>
        <p>
          <span style={{ fontWeight: "600" }}>Date Created </span>{" "}
          <span
            style={{
              opacity: "0.5",
            }}
          >
            05-5-2021
          </span>
        </p>
        <p>
          <span style={{ fontWeight: "600" }}>Time Created </span>{" "}
          <span
            style={{
              opacity: "0.5",
            }}
          >
            10:38 AM
          </span>
        </p>
      </div>
      <div
        style={{
          margin: "0 1.5rem",
          padding: "1rem 0",
        }}
      >
        {information.map((info) => (
          <div
            style={{
              display: "flex",
            }}
          >
            <p
              style={{
                width: "50%",
                fontWeight: "600",
              }}
            >
              {Object.keys(info)}
            </p>
            <p
              style={{
                width: "50%",
              }}
            >
              {Object.values(info)}
            </p>
          </div>
        ))}
      </div>
      <div
        style={{
          position: "absolute",
          bottom: "25px",
          borderTop: "1px solid rgba(231, 231, 237, 0.6)",
          width: "100%",
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <Button
          onClick={() => setShow(false)}
          style={{
            backgroundColor: "#4F1699",
            padding: ".6rem 1.5rem",
            marginRight: "1rem",
            marginTop: "1rem",
          }}
        >
          Update Status
        </Button>
      </div>
    </Modal>
  );
};
export default IssuesModal;
