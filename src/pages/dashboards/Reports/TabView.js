import React from "react";

import Table from "./Table";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Calendar } from "react-feather";

import Statistics from "./Statistics";

const TabView = ({ title, clicked, index, setIndex, number }) => (
  <>
    <div
      style={{
        padding: "3rem 0 1rem 0 ",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          marginBottom: "10px",
        }}
      >
        <p
          style={{
            fontSize: "18px",
            color: "#304762",
          }}
        >
          {title} Transaction Analytics
        </p>
        <UncontrolledDropdown className="d-inline filter-dropdown">
          <DropdownToggle
            caret
            color="light"
            className="shadow-sm"
            style={{
              backgroundColor: "white",
            }}
          >
            <Calendar className="feather align-middle mt-n1" /> Last 30 days
          </DropdownToggle>
          <DropdownMenu
            right
            style={{
              top: "25px",
            }}
          >
            <DropdownItem>Today</DropdownItem>
            <DropdownItem>Last 7 Days</DropdownItem>
            <DropdownItem>Last 90 Days</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Customize</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </div>

      <Statistics />
    </div>
    <div
      style={{
        marginTop: "1rem",
      }}
    >
      <Table
        title={title}
        clicked={clicked}
        index={index}
        setIndex={setIndex}
        number={number}
      />
    </div>
  </>
);
export default TabView;
