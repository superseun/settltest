import React, { useState, useEffect } from "react";
import { TabContent, TabPane } from "reactstrap";
import TabView from "../TabView";
import TransactionDetail from "../TransactionDetail";
import data from "./data";

const AgentsReport = () => {
  const [activeTab, setActiveTab] = useState(0);
  const [showDetails, setShowDetails] = useState(false);
  const [index, setIndex] = useState(null);
  const tabs = [
    "All",
    "Peer-to-Peer",
    "Transfers",
    "Bill Payments",
    "POS Withdrawals",
    "USSD Withdrawals",
    "Wallet top-up",
    "Voucher Withdrawals",
  ];
  const title = [
    "All",
    "P2P",
    "Transfers",
    "Bills",
    "POS Withdrawals",
    "USSD Withdrawals",
    "Wallet top-up",
    "Voucher Withdrawals",
  ];
  return (
    <>
      {!showDetails ? (
        <>
          <div className="bg_absolute">
            <div className="nav">
              {tabs.map((tab, i) => (
                <div
                  onClick={() => setActiveTab(i)}
                  className={`${activeTab === i ? "active_nav" : ""}`}
                >
                  <p>{tab}</p>
                </div>
              ))}
            </div>
          </div>
          <TabContent activeTab={activeTab}>
            {tabs.map((tab, i) => (
              <TabPane tabId={i} key={i}>
                <TabView
                  title={title[i]}
                  clicked={setShowDetails}
                  index={i}
                  setIndex={setIndex}
                  number="Agent"
                />
              </TabPane>
            ))}
          </TabContent>
        </>
      ) : (
        <TransactionDetail
          setShowDetails={setShowDetails}
          title={tabs[index]}
          data={data[index]}
          type="Agent"
        />
      )}
    </>
  );
};

export default AgentsReport;
