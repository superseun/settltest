import React from "react";
import upArrow from "../../../assets/img/icons/uparrow.svg";
import downArrow from "../../../assets/img/icons/downarrow.svg";

const Statistics = () => (
  <div class="container-fluid">
    <div class="row row-cols-4  transaction_analysis_section">
      <div class="col col_border_right">
        <div className="transaction_volume">
          <h4>Total Transaction Volume</h4>
          <p>
            27,000{" "}
            <span>
              +2.5% <img className="img-fluid" src={upArrow} alt="uparrow" />
            </span>{" "}
          </p>
          <p className="time">Analytics for last 30 days</p>
        </div>
      </div>
      <div class="col col_border_right">
        <div className="transaction_volume">
          <h4>Total Transaction Value</h4>
          <p>
            2,770,630{" "}
            <span>
              +2.5% <img className="img-fluid" src={upArrow} alt="uparrow" />
            </span>{" "}
          </p>
          <p className="time">Analytics for last 30 days</p>
        </div>
      </div>
      <div class="col col_border_right">
        <div className="transaction_volume">
          <h4> Pending Transaction</h4>
          <p>
            500{" "}
            <span className="text-danger">
              +1.5%{" "}
              <img className="img-fluid" src={downArrow} alt="downarrow" />
            </span>{" "}
          </p>
          <p className="time">Analytics for last 30 days</p>
        </div>
      </div>
      <div class="col">
        <div className="transaction_volume">
          <h4>Failed Transaction</h4>
          <p>
            7,600{" "}
            <span>
              +1.5% <img className="img-fluid" src={upArrow} alt="uparrow" />
            </span>{" "}
          </p>
          <p className="time">Analytics for last 30 days</p>
        </div>
      </div>
    </div>
  </div>
);

export default Statistics;
