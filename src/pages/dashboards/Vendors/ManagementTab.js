import React, { useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import Modal from "./ManagementModal";
import managementdata from "./managementdata";
import Header from "./TabHeader";

const ManagementTab = () => {
  const [show, setShow] = useState(false);
  const [data, setData] = useState(managementdata);
  const filter = (e) => {
    const { value } = e.target;
    setData(
      managementdata.filter((text) =>
        text.bank.toLocaleLowerCase().startsWith(value)
      )
    );
  };
  const columns = [
    {
      dataField: "bank",
      text: "Banks",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        paddingLeft: "20px",
      },
      style: () => ({
        padding: "20px",
      }),
      formatter: (cell, row, rowIndex) => {
        return (
          <div
            style={{
              display: "flex",
              gap: "10px",
              alignItems: "center",
            }}
          >
            <img
              src={data[rowIndex].img}
              alt={cell}
              style={{
                height: "100%",
              }}
            />
            <span>{cell}</span>
          </div>
        );
      },
    },
    {
      dataField: "vendor",
      text: "Processing Vendors",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
      },
    },
    {
      dataField: "",
      text: "Action",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        width: "200px",
      },
      formatter: (cell, row, rowIndex) => {
        return <span className="update_action">Update Vendor</span>;
      },
      events: {
        onClick: () => {
          setShow(true);
        },
      },
    },
  ];
  const customTotal = (from, to, size) => (
    <span
      className="react-bootstrap-table-pagination-total"
      style={{
        padding: "1rem",
      }}
    >
      Showing {from} to {to} of {size} Results
    </span>
  );
  const rowStyle = {
    border: "none",
    padding: "21px",
  };

  return (
    <ToolkitProvider keyField="id" data={data} columns={columns}>
      {(props) => (
        <div className="table">
          <p className="table_title">All Banks</p>
          <Header placeholder="Find Bank" filter={filter} />
          <BootstrapTable
            {...props.baseProps}
            bordered={false}
            rowStyle={rowStyle}
            pagination={paginationFactory({
              sizePerPage: 9,
              hideSizePerPage: true,
              showTotal: true,
              paginationTotalRenderer: customTotal,
            })}
          />
          <Modal show={show} setShow={setShow} />
        </div>
      )}
    </ToolkitProvider>
  );
};
export default ManagementTab;
