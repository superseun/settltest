import React, { useState } from "react";
import { ReactComponent as Close } from "../../../BgImages/close.svg";
import { Modal, Button } from "react-bootstrap";
import {
  InputGroupAddon,
  Col,
  InputGroup,
  Input,
  CustomInput,
} from "reactstrap";
import { ReactComponent as SearchIcon } from "../../../BgImages/search.svg";
import managementdata from "./managementdata";
const ManagementModal = ({ show, setShow }) => {
  const [data, setData] = useState(managementdata);
  const filter = (e) => {
    const { value } = e.target;
    setData(
      managementdata.filter((text) =>
        text.bank.toLocaleLowerCase().startsWith(value)
      )
    );
  };
  return (
    <Modal
      show={show}
      centered
      onHide={() => setShow(false)}
      className="management-modal"
    >
      <div
        style={{
          display: "relative",
        }}
      >
        <h3
          style={{
            textAlign: "center",
            borderBottom: "1px solid rgba(231, 231, 237, 0.6)",
            padding: "1.5rem 0",
          }}
        >
          Update First City Monument Bank Vendor
        </h3>
        <Close
          style={{
            position: "absolute",
            top: "20px",
            right: "20px",
            cursor: "pointer",
          }}
          onClick={() => setShow(false)}
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Col lg="11">
          <InputGroup
            style={{
              position: "relative",
              margin: 0,
              paddingTop: "0.4rem",
            }}
          >
            <InputGroupAddon addonType="append" color="primary">
              <SearchIcon
                style={{
                  position: "absolute",
                  zIndex: "5",
                  left: "10px",
                  height: "100%",
                  top: "4px",
                }}
              />
            </InputGroupAddon>
            <Input
              style={{
                padding: "20px 35px",
                borderRadius: "8px",
                width: "100%",
                margin: "10px 0",
              }}
              onChange={filter}
              placeholder="Find Vendor"
            />
          </InputGroup>
        </Col>
        <Col lg="11">
          {data.map((bank, i) => (
            <div
              key={i}
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
                padding: "20px 0",
                borderBottom: "  0.5px solid #F6EFFF",
              }}
            >
              <div
                style={{
                  display: "flex",
                  gap: "10px",
                  alignItems: "center",
                  width: "70%",
                }}
              >
                <img
                  src={bank.img}
                  alt={bank.bank}
                  style={{
                    height: "100%",
                  }}
                />
                <span
                  style={{
                    color: "#000000",
                  }}
                >
                  {bank.bank}
                </span>
              </div>
              <CustomInput
                type="radio"
                id="exampleCustomRadio"
                name="customRadio"
                className="custom-radio"
                label=""
                id={`${bank.bank}`}
              />
            </div>
          ))}
        </Col>
      </div>
      <div
        style={{
          position: "absolute",
          bottom: "25px",
          borderTop: "1px solid rgba(231, 231, 237, 0.6)",
          width: "100%",
          display: "flex",
          padding: "0 20px",
          justifyContent: "space-between",
        }}
      >
        <Button
          onClick={() => setShow(false)}
          style={{
            border: "1px solid #DADDE1",
            padding: ".6rem 1.5rem",
            marginRight: "1rem",
            marginTop: "1rem",
            backgroundColor: "white",
            color: "black",
          }}
        >
          Cancel
        </Button>
        <Button
          onClick={() => setShow(false)}
          style={{
            backgroundColor: "#4F1699",
            padding: ".6rem 1.5rem",
            marginRight: "1rem",
            marginTop: "1rem",
            color: "white",
            border: "none",
          }}
        >
          Update Vendor
        </Button>
      </div>
    </Modal>
  );
};
export default ManagementModal;
