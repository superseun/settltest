import { gtb, fcmb, fbn, fidelity, access } from "../../../assets/img/banks";
const data = [
  {
    bank: "Guaranty Trust Bank",
    vendor: "FlutterWave",
    img: gtb,
  },
  {
    bank: "First City Monument Bank",
    vendor: "FlutterWave",
    img: fcmb,
  },
  {
    bank: "Fidelity Bank",
    vendor: "FlutterWave",
    img: fidelity,
  },
  {
    bank: "First Bank of Nigeria",
    vendor: "FlutterWave",
    img: fbn,
  },
  {
    bank: "Access bank",
    vendor: "FlutterWave",
    img: access,
  },
];
export default data;
