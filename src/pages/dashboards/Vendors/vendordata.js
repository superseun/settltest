const data = [
  {
    name: "FlutterWave",
    walletBalance: "NGN 170,000,000.00",
    transaction: "Transfer",
    status: "active",
  },
  {
    name: "FlutterWave",
    walletBalance: "NGN 50,000.00",
    transaction: "Transfer",
    status: "inactive",
  },
  {
    name: "Africa's Talking",
    walletBalance: "NGN 500,000.00",
    transaction: "Airtime",
    status: "active",
  },
];
export default data;
