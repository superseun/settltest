import React from "react";
import TransactionCard from "../../../components/TransactionCard";

const Statistics = () => {
  const transactions = [
    {
      name: "Total Virtual Card Request",
      total_amount: "79,800",
      percentage: "+54",
    },
    {
      name: "Total Physical Card Request",
      total_amount: "600,400",
      percentage: "+54",
    },
    {
      name: "Total Virtual Card Disbursed",
      total_amount: "32,400",
      percentage: "+54",
    },
    {
      name: "Total Physical Card Disbursed",
      total_amount: "12,400",
      percentage: "+34",
    },
    {
      name: "Total Volume of Active Cards",
      total_amount: "12,400",
      percentage: "-10",
    },
    {
      name: "Total Volume of Users with Cards",
      total_amount: "2,400",
      percentage: "+54",
    },
  ];
  return (
    <div className="transaction_body">
      {transactions.map((transaction, i) => (
        <TransactionCard
          key={i}
          name={transaction.name}
          total_amount={transaction.total_amount}
          percentage={transaction.percentage}
        />
      ))}
    </div>
  );
};

export default Statistics;
