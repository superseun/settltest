import React, { useState } from "react";
import Header from "../../../components/Header";
import Statistics from "./Statistics";
import Table from "./Table";
import Customer from "./Customer";
import logo from "../../../assets/img/icons/referralicon.svg";
const CardManagement = () => {
  const [showDetails, setShowDetails] = useState(false);
  return (
    <div>
      {!showDetails ? (
        <>
          <Header logo={logo} name="Card Analytics" calendar />
          <Statistics />
          <Table setShowDetails={setShowDetails} />
        </>
      ) : (
        <Customer setShowDetails={setShowDetails} />
      )}
    </div>
  );
};

export default CardManagement;
