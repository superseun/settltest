import React, { useEffect, useState } from "react";
import { Button } from "reactstrap";
import logo from "../../../assets/img/icons/customercard.svg";
import Modal from "./Modal";
const Customer = ({ setShowDetails }) => {
  const [show, setShow] = useState(false);
  const customerData = [
    { "Customer Phone Number": "09029244116" },
    { "Created at": "Apr 15,2021 19:51:55" },
    { "Customer ID": <p className="name">210415054800036197</p> },
    { "Customer BVN": "22036739027" },
    { "Wallet associated with Card": "Primary Wallet" },
    { "Card Type": "Physical Card" },
  ];
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <>
      <div style={{ display: "flex", gap: "10px", padding: "10px" }}>
        <span
          style={{ color: "#4f1699", cursor: "pointer" }}
          onClick={() => setShowDetails((prev) => !prev)}
        >
          Card Management
        </span>
        <span style={{ color: "#304762" }}>{" > "}</span>{" "}
        <span style={{ color: "#304762" }}>Card Request Details</span>
      </div>
      <div className="customer_card_body">
        <div className="flex">
          <div>
            <h4 className="greeting">
              <img src={logo} className="" alt="Settl Logo" />
              <span className="pl-2">Customer Card Request </span>
            </h4>
            <p className="status pending">Pending</p>
          </div>
          <div className="buttons">
            <Button className="button_decline" onClick={() => setShow(true)}>
              Decline Request
            </Button>
            <Button className="button_approve">Approve Request</Button>
          </div>
        </div>
        <div className="details">
          {customerData.map((info) => (
            <div
              style={{
                display: "flex",
              }}
            >
              <p
                style={{
                  width: "50%",
                  color: "rgba(48, 71, 98, 0.8)",
                }}
              >
                {Object.keys(info)}
              </p>
              <p
                style={{
                  width: "50%",
                  color: "#304762",
                  fontWeight: 500,
                }}
              >
                {Object.values(info)}
              </p>
            </div>
          ))}
        </div>
      </div>
      <Modal show={show} setShow={setShow} />
    </>
  );
};
export default Customer;
