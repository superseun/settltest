import React, { useState } from "react";
import Header from "../../../components/Header";
import Statistics from "./Statistics";
import Table from "./Table";
import TransactionDetails from "./TransactionDetail";
import Tab from "./Tab";
import logo from "../../../assets/img/icons/referralicon.svg";
const WalletSavings = () => {
  const [showDetails, setShowDetails] = useState(false);
  return (
    <div>
      {!showDetails ? (
        <>
          <Header logo={logo} name="Savings Analytics" calendar />
          <Statistics />
          <Tab />
          <Table setShowDetails={setShowDetails} />
        </>
      ) : (
        <TransactionDetails setShowDetails={setShowDetails} />
      )}
    </div>
  );
};

export default WalletSavings;
