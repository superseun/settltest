import React, { useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from "react-bootstrap-table2-toolkit";

import walletData from "./data.js";
import Header from "./TableHeader";
import Loader from "../../../components/Loader";

const Table = ({ setShowDetails }) => {
  const [isfiltered, setIsFiltered] = useState(false);
  const [data, setData] = useState(walletData);
  const currentDate = new Date();
  const currentDateFormat = currentDate.setHours(0, 0, 0, 0);
  const SearchFilter = (data) => {
    let filter = walletData;
    if (data.name === "Transaction Refernce") {
      filter = walletData.filter((product) =>
        product.transRef.toLowerCase().startsWith(data.value.trim())
      );
    }
    if (data.name === "Customer's phone number") {
      filter = walletData.filter((product) =>
        product.phoneNumber.startsWith(data.value.trim())
      );
    }
    setData(filter);
  };
  const Filter = (data) => {
    setData("");
    const check =
      data.status.length > 0 ||
      data.startDate !== currentDateFormat ||
      data.endDate !== currentDateFormat;

    setTimeout(() => {
      const filtered = walletData.filter((product) => {
        if (check) {
          setIsFiltered(true);
          const productDate = new Date(product.dateCreated).setHours(
            0,
            0,
            0,
            0
          );
          return (
            data.status.includes(product.status.toLowerCase()) ||
            (productDate >= data.startDate && productDate <= data.endDate)
          );
        } else {
          setIsFiltered(false);
          return product;
        }
      });
      setData(filtered);
    }, 500);
  };
  const columns = [
    {
      dataField: "id",
      text: "#",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        fontSize: "12px",
        width: "70px",
        textAlign: "center",
      },
      formatter: (cell, row, rowIndex) => {
        return (
          <span className="d-flex justify-content-center">{rowIndex}</span>
        );
      },
    },
    {
      dataField: "name",
      text: "Customer",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        fontSize: "12px",
      },
    },
    {
      dataField: "amount",
      text: "Amount",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        fontSize: "12px",
      },
    },
    {
      dataField: "transRef",
      text: "Tranx Ref",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        fontSize: "12px",
      },
    },
    {
      dataField: "phoneNumber",
      text: "Customer Phone Number",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        fontSize: "11px",
      },
    },
    {
      dataField: "created",
      text: "Created",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        fontSize: "12px",
      },
    },
    {
      dataField: "status",
      text: "Status",
      headerStyle: {
        backgroundColor: "#F8F9FA",
        border: "none",
        fontSize: "12px",
        textAlign: "center",
      },
      style: () => {
        return {
          margin: "auto 0",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: "20px 0",
        };
      },
      formatter: (cell, row, rowIndex) => {
        if (cell.toLowerCase() === "success") {
          return <span className=" status success">{cell}</span>;
        }
        if (cell.toLowerCase() === "pending") {
          return <span className="status pending">{cell}</span>;
        }
        if (cell.toLowerCase() === "failed") {
          return <span className="status failed">{cell}</span>;
        }
      },
    },
  ];
  const customTotal = (from, to, size) => (
    <span
      className="react-bootstrap-table-pagination-total"
      style={{
        padding: "1rem",
      }}
    >
      Showing {from} to {to} of {size} Results
    </span>
  );
  const rowStyle = {
    border: "none",
    cursor: "pointer",
  };
  const rowEvent = {
    onClick: (e, row, rowIndex) => {
      setShowDetails((prev) => !prev);
    },
  };
  return (
    <ToolkitProvider
      responsive
      keyField="id"
      data={data}
      columns={columns}
      exportCSV={{
        fileName: "issue log.csv",
      }}
      search
    >
      {(props) => (
        <div
          className="table"
          style={{
            marginTop: "2rem",
          }}
        >
          <p className="table_title">Savings Transactions</p>
          <Header
            csvData={props.csvProps}
            search={props.searchProps}
            filter={Filter}
            filtered={isfiltered}
            dataLength={data.length}
            SearchFilter={SearchFilter}
          />
          {!data ? (
            <Loader />
          ) : (
            <BootstrapTable
              {...props.baseProps}
              bootstrap4
              bordered={false}
              rowEvents={rowEvent}
              rowStyle={rowStyle}
              pagination={paginationFactory({
                sizePerPage: 9,
                hideSizePerPage: true,
                showTotal: true,
                paginationTotalRenderer: customTotal,
              })}
            />
          )}
        </div>
      )}
    </ToolkitProvider>
  );
};
export default Table;
