import React from "react";
import TransactionCard from "../../../components/TransactionCard";

const Statistics = () => {
  const transactions = [
    {
      name: "Total Volume of Users Saving ",
      total_amount: "79,800",
      percentage: "+54",
    },
    {
      name: "Total Value of all Savings",
      total_amount: "600,400",
      percentage: "+54",
    },
    {
      name: "Total Savings Interest ",
      total_amount: "72,400",
      percentage: "+54",
    },
  ];
  return (
    <div className="transaction_body">
      {transactions.map((transaction, i) => (
        <TransactionCard
          key={i}
          name={transaction.name}
          total_amount={transaction.total_amount}
          percentage={transaction.percentage}
        />
      ))}
    </div>
  );
};

export default Statistics;
