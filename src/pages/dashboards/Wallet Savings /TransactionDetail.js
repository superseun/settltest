import React, { useEffect, useState } from "react";
import { Button } from "reactstrap";
import logo from "../../../assets/img/icons/customercard.svg";
import Modal from "./Modal";

const Customer = ({ setShowDetails }) => {
  const refund = {
    title: `Refund Customer`,
    reason: "Refund Reason",

    placeholder: "Initiate Refund",
  };
  const reprocess = {
    title: "Reprocess Transaction",
    reason: "Reprocess Reason",
    placeholder: "Reprocess",
  };
  const [modalDetail, setModalDetail] = useState(null);
  const [show, setShow] = useState(false);

  const customerData = [
    { "Transaction Name": "Savings" },
    { "Savings Wallet": "Kids" },
    { "Wallet Name": "Promise School Fees" },
    { "Transaction Type": "Credit" },
    { "Transaction Reference": "Debit" },
    { "Created at": "Apr 15,2021 19:51:55" },
    { "Customer ID": <p className="name">210415054800036197</p> },
    { "Customer Wallet ID": "SETTL7OT27ETD923722E6322" },
  ];
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <>
      <div style={{ display: "flex", gap: "10px", padding: "10px" }}>
        <span
          style={{ color: "#4f1699", cursor: "pointer" }}
          onClick={() => setShowDetails((prev) => !prev)}
        >
          All Saving Transactions
        </span>
        <span style={{ color: "#304762" }}>{" > "}</span>{" "}
        <span style={{ color: "#304762" }}>Savings Transaction Details</span>
      </div>
      <div className="customer_card_body">
        <div className="flex">
          <div>
            <div>
              <h4 className="greeting">
                <img src={logo} className="" alt="Settl Logo" />
                <span className="pl-2">Savings Transaction Details </span>
              </h4>
              <div className="flex">
                <p className="price">₦13,500.00</p>
                <p className="status failed">Failed</p>
              </div>
            </div>
          </div>
          <div className="buttons">
            <Button
              className="button_neutral"
              onClick={() => {
                setModalDetail(refund);
                setShow(true);
              }}
            >
              Refund Customer
            </Button>
            <Button
              className="button_approve"
              onClick={() => {
                setModalDetail(reprocess);
                setShow(true);
              }}
            >
              Reprocess Transaction
            </Button>
          </div>
        </div>
        <div className="details">
          {customerData.map((info) => (
            <div
              style={{
                display: "flex",
              }}
            >
              <p
                style={{
                  width: "50%",
                  color: "rgba(48, 71, 98, 0.8)",
                }}
              >
                {Object.keys(info)}
              </p>
              <p
                style={{
                  width: "50%",
                  color: "#304762",
                  fontWeight: 500,
                }}
              >
                {Object.values(info)}
              </p>
            </div>
          ))}
        </div>
      </div>
      {modalDetail && (
        <Modal show={show} setShow={setShow} data={modalDetail} />
      )}
    </>
  );
};
export default Customer;
