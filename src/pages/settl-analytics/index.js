import React, { useState } from "react";
import Header from "../../components/Header";
import logo from "../../assets/img/icons/referralicon.svg";
import Statistics from "./Statistics";
import Chart from "./Chart";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Calendar } from "react-feather";
const SettlAnalytics = () => {
  const [activeTab, setActiveTab] = useState("1");
  return (
    <>
      <div className="bg_absolute">
        <Header name="Reports" logo={logo} />
        <div className="nav">
          <div
            onClick={() => setActiveTab("1")}
            className={`${activeTab === "1" ? "active_nav" : ""}`}
          >
            <p>Setll Customers</p>
          </div>
          <div
            onClick={() => setActiveTab("2")}
            className={`${activeTab === "2" ? "active_nav" : ""}`}
          >
            <p>Settl Agents</p>
          </div>
        </div>
      </div>
      <div
        style={{
          marginTop: "8rem",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: "10px",
          }}
        >
          <p
            style={{
              color: "#304762",
              fontSize: "20px",
            }}
          >
            Settl Transaction Analytics
          </p>
          <UncontrolledDropdown className="d-inline filter-dropdown">
            <DropdownToggle
              caret
              color="light"
              className="shadow-sm"
              style={{
                backgroundColor: "white",
              }}
            >
              <Calendar className="feather align-middle mt-n1" /> Last 30 days
            </DropdownToggle>
            <DropdownMenu
              right
              style={{
                top: "25px",
              }}
            >
              <DropdownItem>Today</DropdownItem>
              <DropdownItem>Last 7 Days</DropdownItem>
              <DropdownItem>Last 90 Days</DropdownItem>
              <DropdownItem divider />
              <DropdownItem>Customize</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </div>

        <Statistics />
        <div
          style={{
            marginTop: "2rem",
          }}
        >
          <Chart />
        </div>
      </div>
    </>
  );
};
export default SettlAnalytics;
