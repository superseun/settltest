import React, { useState } from "react";
import Chart from "react-apexcharts";
import { connect } from "react-redux";
import {
  Col,
  Row,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Dropdown,
} from "reactstrap";
import { Calendar } from "react-feather";
import { Card, CardBody, CardHeader, CardTitle } from "reactstrap";

const ColumnChart = ({ theme }) => {
  const transactions = ["Transaction Breakdown", "Revenue Breakdown"];
  const [dropdownOpen, setOpen] = useState(false);
  const [transaction, setTransaction] = useState(transactions[0]);
  const toggle = () => setOpen(!dropdownOpen);
  const data = [
    {
      name: "Bill Payment",
      data: [44, 55, 57, 56, 61, 58, 63, 60, 66],
    },
    {
      name: "Peer-to-Peer",
      data: [76, 85, 101, 98, 87, 105, 91, 114, 94],
    },
    {
      name: "Savings",
      data: [35, 41, 36, 26, 45, 48, 52, 53, 41],
    },
    {
      name: "Transfer",
      data: [35, 41, 36, 26, 45, 48, 52, 53, 41],
    },
    {
      name: "Money Request",
      data: [35, 41, 36, 26, 45, 48, 52, 53, 41],
    },
  ];

  const options = {
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: "60%",
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      show: true,
      width: 2,
      colors: ["transparent"],
    },
    xaxis: {
      categories: [
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
      ],
    },

    fill: {
      opacity: 1,
    },
    tooltip: {
      y: {
        formatter: function (val) {
          return "$ " + val + " thousands";
        },
      },
    },
    colors: ["#3DBDC8", "#27A0C7", "#DB74E4", "#61B3FF", "#9C6ADE"],
  };

  return (
    <Card>
      <CardHeader className="d-flex">
        <CardTitle tag="h5">
          <Dropdown
            isOpen={dropdownOpen}
            toggle={toggle}
            style={{
              outline: "none",
            }}
          >
            <DropdownToggle
              style={{
                border: "none",
                display: "flex",
                gap: "5px",
                alignItems: "center",
                color: "black",
                backgroundColor: "white",
              }}
              caret
            >
              {transaction}
              <DropdownMenu
                className="dropdown_menu"
                style={{
                  padding: ".5rem 0.3rem",
                  width: "fit-content",
                  textAlign: "center",
                }}
              >
                <p
                  className="dropdown_option"
                  onClick={() => setTransaction(transactions[0])}
                >
                  {" "}
                  {transactions[0]}
                </p>
                <p
                  className="dropdown_option"
                  onClick={() => setTransaction(transactions[1])}
                >
                  {transactions[1]}
                </p>
              </DropdownMenu>
            </DropdownToggle>
          </Dropdown>
        </CardTitle>
        <Col xs="auto" className="ml-auto text-right mt-n1">
          <UncontrolledDropdown className="d-inline filter-dropdown">
            <DropdownToggle
              caret
              color="light"
              className="shadow-sm"
              style={{
                backgroundColor: "white",
              }}
            >
              <Calendar className="feather align-middle mt-n1" /> Jan 04, 2019 -
              Dec 04 2019
            </DropdownToggle>
            <DropdownMenu
              right
              style={{
                top: "25px",
              }}
            >
              <DropdownItem>Today</DropdownItem>
              <DropdownItem>Last 7 Days</DropdownItem>
              <DropdownItem>Last 90 Days</DropdownItem>
              <DropdownItem divider />
              <DropdownItem>Customize</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Col>
      </CardHeader>

      <CardBody>
        <div className="chart">
          <Chart options={options} series={data} type="bar" height="350" />
        </div>
      </CardBody>
    </Card>
  );
};

export default connect((store) => ({
  theme: store.theme.currentTheme,
}))(ColumnChart);
