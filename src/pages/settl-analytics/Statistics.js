import React from "react";
import TransactionCard from "../../components/TransactionCard";

const Statistics = ({ index }) => {
  const transactions = [
    {
      name: "Total Transaction Volume",
      total_amount: "79,800",
      percentage: "+54",
    },
    {
      name: "Total Transaction Value",
      total_amount: "600,400",
      percentage: "+54",
    },
    {
      name: "Total Settl Customers",
      total_amount: "32,400",
      percentage: "+54",
    },
  ];
  return (
    <div className="transaction_body">
      {transactions.map((transaction, i) => (
        <TransactionCard
          key={i}
          name={transaction.name}
          total_amount={transaction.total_amount}
          percentage={transaction.percentage}
          icon={transaction.icon}
        />
      ))}
    </div>
  );
};

export default Statistics;
