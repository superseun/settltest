import React from 'react';
import usePasswordToggle from '../../Hooks/usePasswordToggle';
import ForgetPassword from '../../components/Auth/ForgetPasword'
import logo from "../../assets/img/logo/Settl logo.svg";
import appStore from "../../assets/img/logo/App Store.svg";
import googleStore from "../../assets/img/logo/Google Store.svg";
import settlScreenshot from "../../assets/img/screenshots/settl_screenshot.svg";
import ForgetPaswordOTP from '../../components/Auth/ForgetPaswordOTP';



const Login = () => {
    const [PasswordInutType,  ToggleIcon] = usePasswordToggle();
    

    return (
        <div id="" className="container-fluid">
            <div id="login_section" className="container">
                <div className="row">
                    <div className="col-md-6">
                       <div className="auth_right">
                        <h1 className="text-center">Easy & Reliable <br/> Banking Platform</h1>
                        <div className="text-center">
                          <img src={settlScreenshot} alt="settl screenshot"/>
                        </div>
                        <div className="text-center downloadApps">
                            <img src={appStore} alt="App Store"/>
                            <img  src={googleStore} alt="Google Store"/>
                        </div>
                       </div>
                    </div>
                    <div className="col-md-6 login_screen">
                        <div className="text-center login_screen_input">
                          <img src={logo} alt="settl Logo"/>
                          <p>Welcome back, log in to continue</p>
                        
                            <form className="text-center p-4" action="#!">
                                <div className="group">
                                    <input 
                                        type="email" 
                                        placeholder="Email Address" 
                                        className="form_in shadow-none" 
                                        id="email"
                                    />
                                    <label 
                                        htmlFor="Email_Address">
                                        Email Address
                                    </label>
                                </div>

                                <div className="group">
                                    <input 
                                        type={PasswordInutType} 
                                        className="form_in shadow-none" 
                                        placeholder="Password" 
                                        id="password"
                                    />
                                    <label 
                                        htmlFor="password">
                                        Password
                                    </label>
                                    <span className="password-toggle-icon">{ ToggleIcon }</span>
                                </div>
                                <div className="d-flex justify-content-around remember_me">
                                    <div>
                                        <div className="custom-control custom-checkbox">
                                            <input
                                                type="checkbox" 
                                                className="custom-control-input shadow-none" 
                                                id="defaultLoginFormRemember"
                                            />
                                            <label 
                                                className="custom-control-label" 
                                                htmlFor="defaultLoginFormRemember">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
                                    <div className="forget_login">
                                        {/* <ForgetPassword/> */}
                                        <ForgetPaswordOTP/>
                                    </div>
                                </div>
                                <button disabled="" className="btn" type="submit">Login</button>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login
