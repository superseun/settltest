import {useState} from 'react';
import PasswordStrengthMeter from '../../components/Auth/PasswordStrengthMeter';
import "./ResetPassword.css";
import usePasswordToggle from '../../Hooks/usePasswordToggle';


  const ResetPassword = () => {
  const [PasswordInutType,  ToggleIcon] = usePasswordToggle();
  const [ConfirmPasswordInutType,  ConfirmToggleIcon] = usePasswordToggle();
  const [password,  setPassword] = useState('');
  const [confirmPassword,  setConfirmPassword] = useState('');
  const match = null;
  function handleClick(e) {
    e.preventDefault();
    if(password === confirmPassword){
       console.log(' "Oops! Passwords don\'t match."');
    } 
    
  }

    return (
        <div id="auth_bg" className="container-fluid">
            <div id="login_section" className="container">
                <div className="row">
                    <div className="col-md-6">
                       <div className="auth_right">
                        <h1 className="text-center">Easy & Reliable <br/> Banking Platform</h1>
                        <div className="text-center">
                          <img src="assets/images/settl_screenshot.svg" alt="settl screenshot"/>
                        </div>
                        <div className="text-center downloadApps">
                            <img src="assets/images/App Store.svg" alt="App Store"/>
                            <img src="assets/images/Google Store.svg" alt="Google Store"/>
                        </div>
                       </div>
                    </div>
                    <div className="col-md-6 login_screen">
                      <form>
                        <div className="text-center login_screen_input">
                          <img src="assets/images/whitelogo.svg" alt="settl Logo"/>
                          
                            <div className="reset_password_header"> 
                            <h1>Reset Password</h1>
                            <p>For proper security we require a minimum of 8 characters with at least 1 uppercase, 1 lowercase, and 1 digit.</p>
                            <fieldset className="reset_password_label">
                            <input 
                                name="password" 
                                autoComplete="off" 
                                type={PasswordInutType} 
                                className="form-control shadow-none" 
                                required
                                onChange={e => setPassword(e.target.value)}
                            />
                            <label 
                              htmlFor="new_password"
                            >
                              New password
                            </label>
                            <span className="reset_password_toggle_icon">
                              { ToggleIcon }
                            </span>
                            </fieldset>
                              <PasswordStrengthMeter password={password}/>
                            <fieldset className="reset_password_label">
                            <input 
                                name="confirmPassword" 
                                autoComplete="off" 
                                type={ConfirmPasswordInutType} 
                                className="form-control shadow-none" 
                                required
                                  value={confirmPassword}
                                onChange={e => setConfirmPassword(e.target.value)}
                            />
                            <label 
                             htmlFor="Confirm_password"
                            >
                              Confirm password 
                            </label>
                            <span className="reset_password_toggle_icon">{ ConfirmToggleIcon }</span>
                            </fieldset>
                        </div>
                        {/* <PasswordStrengthBar password={password} /> */}
                             <button type="submit" className="btn reset_password_btn">Reset Password </button>
                           <h1></h1>
                           <a href="#" onClick={handleClick}>
                              Click me
                           </a>
                        </div>
                      </form>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}

export default ResetPassword
